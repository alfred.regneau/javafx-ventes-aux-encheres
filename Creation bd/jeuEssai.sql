
-- les roles
insert into ROLE(idRole,nomRole) values
	(1,'Administrateur'),
	(2,'Utilisateur');

-- les catégories
insert into CATEGORIE(idCat,nomCat) values
	(1,'Vêtement'),
	(2,'Ustensile Cuisine'),
	(3,'Meuble'),
	(4,'Outil');

-- les statuts
insert into STATUT(idSt,nomSt) values
	(1,'A venir'),
	(2,'En cours'),
	(3,'A valider'),
	(4,'Validée'),
	(5,'Non conclue');

-- les utilisateurs
insert into UTILISATEUR(idUt,pseudoUt,emailUT,mdpUt,activeUt,idRole) values
	(1,'mate73','mate73@univ-orleans.fr','mate73','O',1),
	(2,'becoitan754','becoitan754@live.fr','becoitan754','O',1),
	(3,'donrenly96','donrenly96@gmail.com','donrenly96','O',1),
	(4,'novou1','novou1@orange.fr','novou1','N',2),
	(5,'faute669','faute669@gmail.com','faute669','N',2),
	(6,'vopo5','vopo5@live.fr','vopo5','N',2),
	(7,'benvengon4','benvengon4@univ-orleans.fr','benvengon4','O',2),
	(8,'denmyny','denmyny@gmail.com','denmyny','O',2),
	(9,'gydo13','gydo13@univ-orleans.fr','gydo13','O',2),
	(10,'voiry07','voiry07@free.fr','voiry07','O',2),
	(11,'jyken519','jyken519@orange.fr','jyken519','O',2),
	(12,'lonno','lonno@free.fr','lonno','O',2),
	(13,'vaumusy12','vaumusy12@free.fr','vaumusy12','O',2),
	(14,'soumatau99','soumatau99@orange.fr','soumatau99','O',2),
	(15,'mauco68','mauco68@free.fr','mauco68','O',2),
	(16,'diboinoi41','diboinoi41@gmail.com','diboinoi41','O',2),
	(17,'fepa92','fepa92@live.fr','fepa92','O',2),
	(18,'vamu157','vamu157@orange.fr','vamu157','O',2),
	(19,'kauboji','kauboji@univ-orleans.fr','kauboji','O',2),
	(20,'lulau','lulau@univ-orleans.fr','lulau','O',2),
	(21,'given42','given42@gmail.com','given42','O',2),
	(22,'renpopu822','renpopu822@live.fr','renpopu822','O',2),
	(23,'loulou030','loulou030@free.fr','loulou030','O',2),
	(24,'cemou953','cemou953@gmail.com','cemou953','O',2),
	(25,'paton55','paton55@gmail.com','paton55','O',2),
	(26,'sytiro5','sytiro5@live.fr','sytiro5','O',2),
	(27,'palydon627','palydon627@univ-orleans.fr','palydon627','O',2),
	(28,'dacen65','dacen65@free.fr','dacen65','O',2),
	(29,'gapenfi24','gapenfi24@free.fr','gapenfi24','O',2),
	(30,'cyfupa242','cyfupa242@orange.fr','cyfupa242','O',2),
	(31,'suto26','suto26@free.fr','suto26','O',2),
	(32,'gykon73','gykon73@gmail.com','gykon73','O',2),
	(33,'gujoi','gujoi@univ-orleans.fr','gujoi','O',2),
	(34,'jemougan206','jemougan206@univ-orleans.fr','jemougan206','O',2),
	(35,'nanken1','nanken1@free.fr','nanken1','O',2),
	(36,'fauty83','fauty83@univ-orleans.fr','fauty83','O',2),
	(37,'vavonron7','vavonron7@gmail.com','vavonron7','O',2),
	(38,'podoiman4','podoiman4@free.fr','podoiman4','O',2),
	(39,'vansi2','vansi2@free.fr','vansi2','O',2),
	(40,'tenfi','tenfi@orange.fr','tenfi','O',2),
	(41,'benma5','benma5@univ-orleans.fr','benma5','O',2),
	(42,'lacume7','lacume7@orange.fr','lacume7','O',2),
	(43,'bauno30','bauno30@live.fr','bauno30','O',2),
	(44,'detoti01','detoti01@univ-orleans.fr','detoti01','O',2),
	(45,'tounade5','tounade5@univ-orleans.fr','tounade5','O',2),
	(46,'fanesy','fanesy@univ-orleans.fr','fanesy','O',2),
	(47,'jufonran18','jufonran18@gmail.com','jufonran18','O',2),
	(48,'mypouco15','mypouco15@orange.fr','mypouco15','O',2),
	(49,'toibujen579','toibujen579@orange.fr','toibujen579','O',2),
	(50,'sonlon','sonlon@gmail.com','sonlon','O',2),
	(51,'banfoncu80','banfoncu80@univ-orleans.fr','banfoncu80','O',2),
	(52,'poudi756','poudi756@live.fr','poudi756','O',2),
	(53,'beka49','beka49@free.fr','beka49','O',2),
	(54,'faubenvu','faubenvu@free.fr','faubenvu','O',2),
	(55,'rengau708','rengau708@gmail.com','rengau708','O',2),
	(56,'cimou','cimou@live.fr','cimou','O',2),
	(57,'noupon9','noupon9@univ-orleans.fr','noupon9','O',2),
	(58,'madeky070','madeky070@gmail.com','madeky070','O',2),
	(59,'jousannen04','jousannen04@gmail.com','jousannen04','O',2),
	(60,'noifupan426','noifupan426@gmail.com','noifupan426','O',2),
	(61,'cylonvon4','cylonvon4@free.fr','cylonvon4','O',2),
	(62,'depou321','depou321@univ-orleans.fr','depou321','O',2),
	(63,'jikoukon48','jikoukon48@gmail.com','jikoukon48','O',2),
	(64,'letau248','letau248@free.fr','letau248','O',2),
	(65,'cosoi70','cosoi70@free.fr','cosoi70','O',2),
	(66,'koimau','koimau@live.fr','koimau','O',2),
	(67,'doujyloi611','doujyloi611@free.fr','doujyloi611','O',2),
	(68,'gipauji','gipauji@univ-orleans.fr','gipauji','O',2),
	(69,'ponan8','ponan8@free.fr','ponan8','O',2),
	(70,'cannuge04','cannuge04@live.fr','cannuge04','O',2),
	(71,'sikasa7','sikasa7@free.fr','sikasa7','O',2),
	(72,'dinan453','dinan453@univ-orleans.fr','dinan453','O',2),
	(73,'jaufa73','jaufa73@gmail.com','jaufa73','O',2),
	(74,'vankevu','vankevu@live.fr','vankevu','O',2),
	(75,'jime3','jime3@orange.fr','jime3','O',2),
	(76,'goitoikon52','goitoikon52@orange.fr','goitoikon52','O',2),
	(77,'codise64','codise64@live.fr','codise64','O',2),
	(78,'venli6','venli6@free.fr','venli6','O',2),
	(79,'bymegu317','bymegu317@orange.fr','bymegu317','O',2),
	(80,'nentalou4','nentalou4@univ-orleans.fr','nentalou4','O',2),
	(81,'nysau55','nysau55@live.fr','nysau55','O',2),
	(82,'jumauvy0','jumauvy0@univ-orleans.fr','jumauvy0','O',2),
	(83,'syji83','syji83@gmail.com','syji83','O',2),
	(84,'koguno697','koguno697@free.fr','koguno697','O',2),
	(85,'senfau','senfau@gmail.com','senfau','O',2),
	(86,'kefy60','kefy60@gmail.com','kefy60','O',2),
	(87,'nynou','nynou@orange.fr','nynou','O',2),
	(88,'gaunou1','gaunou1@orange.fr','gaunou1','O',2),
	(89,'souroini53','souroini53@orange.fr','souroini53','O',2),
	(90,'varico258','varico258@orange.fr','varico258','O',2),
	(91,'nouvaugoi18','nouvaugoi18@gmail.com','nouvaugoi18','O',2),
	(92,'baryme08','baryme08@live.fr','baryme08','O',2),
	(93,'ridonmy7','ridonmy7@live.fr','ridonmy7','O',2),
	(94,'ranlevou90','ranlevou90@free.fr','ranlevou90','O',2),
	(95,'lifi','lifi@gmail.com','lifi','O',2),
	(96,'gulo','gulo@live.fr','gulo','O',2),
	(97,'roitonka','roitonka@gmail.com','roitonka','O',2),
	(98,'modynen8','modynen8@free.fr','modynen8','O',2),
	(99,'ruben134','ruben134@gmail.com','ruben134','O',2),
	(100,'tefuran2','tefuran2@live.fr','tefuran2','O',2);

-- les objets
insert into OBJET(idOb,nomOb,descriptionOb,idCat,idUt) values
	(1,'Marteau jamais servi','Marteau jamais serviLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',4,95),
	(2,'Cocotte jamais servie','Cocotte jamais servieLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',2,46),
	(3,'Perceuse jaune','Perceuse jauneLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',4,65),
	(4,'Cocotte bleue','Cocotte bleueLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',2,77),
	(5,'Chaise jamais servie','Chaise jamais servieLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',3,35),
	(6,'Chapeau bleu','Chapeau bleuLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',1,72),
	(7,'Perceuse verte','Perceuse verteLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',4,9),
	(8,'Marteau jamais servi','Marteau jamais serviLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',4,31),
	(9,'Buffet bleu','Buffet bleuLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',3,21),
	(10,'T-shirt rouge','T-shirt rougeLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',1,33),
	(11,'T-shirt de qualité','T-shirt de qualitéLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',1,7),
	(12,'Robot ménager jaune','Robot ménager jauneLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',2,79),
	(13,'Cocotte comme neuve','Cocotte comme neuveLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',2,93),
	(14,'Pantalon comme neuf','Pantalon comme neufLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',1,35),
	(15,'Pantalon jaune','Pantalon jauneLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',1,20),
	(16,'Commode bleue','Commode bleueLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',3,13),
	(17,'Armoire jaune','Armoire jauneLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',3,39),
	(18,'T-shirt rouge','T-shirt rougeLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',1,72),
	(19,'Casserole verte','Casserole verteLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',2,90),
	(20,'Chemise comme neuve','Chemise comme neuveLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',1,94),
	(21,'Chaise de qualité','Chaise de qualitéLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',3,98),
	(22,'Scie jaune','Scie jauneLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',4,69),
	(23,'Tondeuse jamais servie','Tondeuse jamais servieLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',4,33),
	(24,'Armoire de qualité','Armoire de qualitéLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',3,13),
	(25,'Marteau rouge','Marteau rougeLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',4,76),
	(26,'Chaise jamais servie','Chaise jamais servieLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',3,83),
	(27,'T-shirt jaune','T-shirt jauneLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',1,94),
	(28,'Chaise jamais servie','Chaise jamais servieLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',3,45),
	(29,'Cocotte jamais servie','Cocotte jamais servieLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',2,67),
	(30,'Robe rouge','Robe rougeLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum scelerisque.
Donec facilisis, ligula vel posuere cursus, sapien leo dictum nisi, interdum aliquam sem nisi ac purus. Curabitur quis quam mauris.',1,60);

-- les ventes
insert into VENTE(idVe,prixBase,prixMin,debutVe,finVe,idSt,idOb) values
	(1,84,92.4,STR_TO_DATE('24/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),STR_TO_DATE('26/05/2019:16:30:00','%d/%m/%Y:%H:%i:%s'),4,11),
	(2,50,55.0,STR_TO_DATE('25/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),STR_TO_DATE('27/05/2019:16:30:00','%d/%m/%Y:%H:%i:%s'),4,12),
	(3,34,37.4,STR_TO_DATE('26/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),STR_TO_DATE('28/05/2019:16:30:00','%d/%m/%Y:%H:%i:%s'),4,13);

-- les enchères
insert into ENCHERIR(idUT,idVe,dateheure,montant) values
	(43,1,date_add(STR_TO_DATE('24/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 44 minute),107),
	(60,1,date_add(STR_TO_DATE('24/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 49 minute),130),
	(95,1,date_add(STR_TO_DATE('24/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 142 minute),223),
	(98,1,date_add(STR_TO_DATE('24/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 149 minute),316),
	(33,2,date_add(STR_TO_DATE('25/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 45 minute),80),
	(19,2,date_add(STR_TO_DATE('25/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 74 minute),145),
	(62,2,date_add(STR_TO_DATE('25/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 75 minute),240),
	(33,2,date_add(STR_TO_DATE('25/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 170 minute),257),
	(50,3,date_add(STR_TO_DATE('26/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 50 minute),71),
	(92,3,date_add(STR_TO_DATE('26/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 127 minute),120),
	(18,3,date_add(STR_TO_DATE('26/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 209 minute),140),
	(26,3,date_add(STR_TO_DATE('26/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 320 minute),226),
	(23,3,date_add(STR_TO_DATE('26/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 358 minute),313);

-- les messages
insert into MESSAGE(idMsg,dateMsg,contenuMsg,luMsg,idOb,idUt_exp,idUt_dest) values
	(1,date_add(STR_TO_DATE('20/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 140 minute),'Il est un peu abimé sur le coté?','O',10,57,33),
	(2,date_add(STR_TO_DATE('20/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 347 minute),'Oui tout à fait.','O',10,33,57),
	(3,date_add(STR_TO_DATE('20/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 639 minute),'Il est un peu abimé sur le coté?','O',10,74,33),
	(4,date_add(STR_TO_DATE('20/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 727 minute),'Mais pas du tout!','O',10,33,74),
	(13,date_add(STR_TO_DATE('21/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1055 minute),'Il est un peu cher non?','O',11,24,7),
	(14,date_add(STR_TO_DATE('21/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1299 minute),'Oui tout à fait.','O',11,7,24),
	(15,date_add(STR_TO_DATE('21/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1660 minute),'Est-ce vous le livrez à domicile','O',11,81,7),
	(16,date_add(STR_TO_DATE('21/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 1810 minute),'Mais pas du tout!','O',11,7,81),
	(25,date_add(STR_TO_DATE('22/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 2191 minute),'Il est un peu cher non?','O',12,30,79),
	(26,date_add(STR_TO_DATE('22/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 2433 minute),'Je ne pense pas.','O',12,79,30),
	(27,date_add(STR_TO_DATE('22/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 2495 minute),'Est-ce vous le livrez à domicile','O',12,76,79),
	(28,date_add(STR_TO_DATE('22/05/2019:10:00:00','%d/%m/%Y:%h:%i:%s'),interval 2677 minute),'ça dépend.','O',12,79,76);
