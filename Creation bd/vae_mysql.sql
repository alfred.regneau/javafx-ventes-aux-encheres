-- CREATE DATABASE IF NOT EXISTS VAE DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
-- USE VAE;

DROP TABLE MESSAGE;
DROP TABLE PHOTO;
DROP TABLE ENCHERIR;
DROP TABLE VENTE;
DROP TABLE STATUT;
DROP TABLE DOCUMENT;
DROP TABLE OBJET;
DROP TABLE UTILISATEUR;
DROP TABLE CATEGORIE;
DROP TABLE ROLE;

CREATE TABLE ROLE (
  idRole decimal(2,0),
  nomRole varchar(20),
  PRIMARY KEY (idRole)
);

CREATE TABLE MESSAGE (
  idMsg decimal(6,0),
  dateMsg datetime,
  contenuMsg text,
  luMsg char(1),
  idOb decimal(6,0),
  idUt_exp decimal(6,0),
  idUt_dest decimal(6,0),
  PRIMARY KEY (idMsg)
);

CREATE TABLE PHOTO (
  idPh decimal(6,0),
  titrePh varchar(50),
  imgPh blob,
  idOb decimal(6,0),
  PRIMARY KEY (idPh)
);

CREATE TABLE UTILISATEUR (
  idUt decimal(6,0),
  pseudoUt varchar(20) unique,
  emailUt varchar(100),
  mdpUt varchar(100),
  activeUt char(1),
  idRole decimal(2,0),
  PRIMARY KEY (idUt)
);

CREATE TABLE OBJET (
  idOb decimal(6,0),
  nomOb varchar(50),
  descriptionOb text,
  idCat decimal(3,0),
  idUt decimal(6,0),
  PRIMARY KEY (idOb)
);

CREATE TABLE CATEGORIE (
  idCat decimal(3,0),
  nomCat varchar(50),
  PRIMARY KEY (idCat)
);


CREATE TABLE ENCHERIR (
  idUt decimal(6,0),
  idVe decimal(8,0),
  dateheure datetime,
  montant decimal(8,2),
  PRIMARY KEY (idUt, idVe, dateheure)
);

CREATE TABLE DOCUMENT (
  idDoc decimal(8,0),
  titreDoc varchar(50),
  contenuDoc blob,
  dateDoc date,
  idUt decimal(6,0),
  idOb decimal(6,0),
  PRIMARY KEY (idDoc)
);

CREATE TABLE STATUT (
  idSt char,
  nomSt varchar(15),
  PRIMARY KEY (idSt)
);

CREATE TABLE VENTE (
  idVe decimal(8,0),
  prixBase decimal(8,2),
  prixMin decimal(8,2),
  debutVe datetime,
  finVe datetime,
  idSt char,
  idOb decimal(6,0),
  PRIMARY KEY (idVe)
);

ALTER TABLE MESSAGE ADD FOREIGN KEY (idUt_exp) REFERENCES UTILISATEUR (idUt);
ALTER TABLE MESSAGE ADD FOREIGN KEY (idUt_dest) REFERENCES UTILISATEUR (idUt);
ALTER TABLE MESSAGE ADD FOREIGN KEY (idOb) REFERENCES OBJET (idOb);
ALTER TABLE PHOTO ADD FOREIGN KEY (idOb) REFERENCES OBJET (idOb);
ALTER TABLE UTILISATEUR ADD FOREIGN KEY (idRole) REFERENCES ROLE (idRole);
ALTER TABLE OBJET ADD FOREIGN KEY (idUt) REFERENCES UTILISATEUR (idUt);
ALTER TABLE OBJET ADD FOREIGN KEY (idCat) REFERENCES CATEGORIE (idCat);
ALTER TABLE ENCHERIR ADD FOREIGN KEY (idVe) REFERENCES VENTE (idVe);
ALTER TABLE ENCHERIR ADD FOREIGN KEY (idUt) REFERENCES UTILISATEUR (idUt);
ALTER TABLE DOCUMENT ADD FOREIGN KEY (idOb) REFERENCES OBJET (idOb);
ALTER TABLE DOCUMENT ADD FOREIGN KEY (idUt) REFERENCES UTILISATEUR (idUt);
ALTER TABLE VENTE ADD FOREIGN KEY (idOb) REFERENCES OBJET (idOb);
ALTER TABLE VENTE ADD FOREIGN KEY (idSt) REFERENCES STATUT (idSt);

