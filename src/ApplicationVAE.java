import controleurs.GestionnairesFenetres;
import interfaces.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;

public class ApplicationVAE extends Application
{
    @Override
    public void start(Stage stage)
    {
        try
        {
            GestionnaireSQL.init();
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("Impossible de trouver le driver SQL, fermeture de l'application...");
            e.printStackTrace();
            System.exit(1);
        }
        catch (SQLException e)
        {
            System.out.println("Erreur lors de la connexion à la base de données, fermeture de l'application...");
            e.printStackTrace();
            System.exit(1);
        }

        GestionnairesFenetres.initStage(stage);
        stage.getIcons().add(new Image("images/icones/home.png"));


    }

    @Override
    public void stop()
    {
        try
        {
            GestionnaireSQL.fin();
        }
        catch(SQLException e)
        {
            System.out.println("Erreur lors de la fermeture de la connexion avec la base de données !");
            e.printStackTrace();
        }

    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
