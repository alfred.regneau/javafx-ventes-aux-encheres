package interfaces;

import controleurs.ControleurEnvoyerMessage;
import controleurs.GestionnairesFenetres;
import interfaces.util.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import modeles.ConnexionUtilisateur;
import modeles.Conversation;
import modeles.Message;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;
import java.text.SimpleDateFormat;


public class ConsulterConversation extends InterfaceModeConnecte
{

    private TextField textTape;
    private Button envoyerMessage;
    private Conversation conversation;
    private VBox conteneurMsg;
    private ScrollPane scrollPane;

    public TextField getTextTape()
    {
        return textTape;
    }

    public Conversation getConversation()
    {
        return conversation;
    }

    public ConsulterConversation(Conversation conversation)
    {
        super();
        this.textTape = new TextField();
        this.envoyerMessage = new Button("Envoyer");
        ControleurEnvoyerMessage envoieMsg = new ControleurEnvoyerMessage(this);
        this.envoyerMessage.setOnAction(envoieMsg);
        this.textTape.setPromptText("Saisir le message...");
        this.textTape.setOnAction(envoieMsg);
        this.conversation = conversation;

        try
        {
            Requetes.marquerConversationCommeLue(GestionnaireSQL.getConnexion(), conversation);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur lors de la mise à jour des messages", "Erreur : les messages n'ont pas pu être marqués comme lus.");
        }
    }

    public void envoiMessage()
    {
        this.textTape.clear();
        majAffiche();
    }

    @Override
    protected Node getContentPane()
    {
        BoutonIconeTexte btnRetour = new BoutonIconeTexte(new Image("images/icones/back.png"), "Retour", 40, new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                GestionnairesFenetres.afficherDernierePage();
            }
        });

        BarreActions actions = new BarreActions(conversation.getObjetConcerne().getNom() + " (" + conversation.getCorrespondant().getPseudo() + ")", btnRetour);


        VBox contenu = new VBox();
        VBox.setVgrow(contenu, Priority.ALWAYS);
        this.scrollPane = zoneConsulterMessage();
        majAffiche();
        VBox.setVgrow(this.scrollPane, Priority.ALWAYS);
        HBox zoneMessage = zoneEcrireMessage();
        contenu.getChildren().addAll(actions, this.scrollPane, zoneMessage);
        return contenu;
    }

    public HBox zoneEcrireMessage()
    {
        HBox barreMsg = new HBox();

        barreMsg.setPadding(new Insets(5, 30, 7, 30));

        TextField ecrireMsg = this.textTape;
        Button envoye = this.envoyerMessage;

        ecrireMsg.setPromptText("Saisir le message...");
        barreMsg.setSpacing(10);
        ecrireMsg.setMinWidth(600);
        envoye.setAlignment(Pos.CENTER_RIGHT);
        barreMsg.getChildren().addAll(ecrireMsg, envoye);
        return barreMsg;
    }

    public ScrollPane zoneConsulterMessage()
    {
        ScrollPane zoneMessage = new ScrollPane();
        Styles.ajouterStyle(zoneMessage, Styles.SCROLLPANE_BACKGROUND);
        zoneMessage.setPadding(new Insets(5, 30, 0, 30));
        zoneMessage.setFitToWidth(true);
        zoneMessage.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        this.conteneurMsg = new VBox();
        this.conteneurMsg.setSpacing(5);

        zoneMessage.setContent(this.conteneurMsg);
        return zoneMessage;
    }


    public void majAffiche()
    {
        this.conteneurMsg.getChildren().clear();
        for (Message msg : getConversation().getMessages())
        {
            VBox vboxMsg = new VBox();
            Label dateMsg = new Label((new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(msg.getDate())));
            Styles.ajouterStyle(dateMsg, Styles.MESSAGERIE_DATE);
            Label contenuMsg = new Label(msg.getContenu());


            if (msg.getExpediteur().getId() == ConnexionUtilisateur.getIdUtilisateurConnecte())
            {
                //va a droite
                VBox.setMargin(vboxMsg, new Insets(0, 0, 0, 350));
                vboxMsg.setAlignment(Pos.TOP_RIGHT);
                contenuMsg.setBackground(Styles.backgroundColor(Color.LIGHTCORAL));
            }
            else
            {
                //va a gauche
                VBox.setMargin(vboxMsg, new Insets(0, 350, 0, 0));
                vboxMsg.setAlignment(Pos.TOP_LEFT);
                contenuMsg.setBackground(Styles.backgroundColor(Color.LIGHTGRAY));
            }

            contenuMsg.setWrapText(true);
            contenuMsg.setPadding(new Insets(5));
            dateMsg.setPadding(new Insets(3, 0, 0, 0));

            vboxMsg.getChildren().addAll(contenuMsg, dateMsg);
            this.conteneurMsg.getChildren().add(vboxMsg);
        }

        this.scrollPane.setVvalue(1.0);
        this.scrollPane.setVvalue(1.0);
    }


}
