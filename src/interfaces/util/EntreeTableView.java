package interfaces.util;

import controleurs.ControleurAccesObjet;
import javafx.scene.control.Hyperlink;
import modeles.Objet;

public class EntreeTableView {
    private String name;
    private String description;
    private Hyperlink details;

    public EntreeTableView(Objet o)
    {
        this.name = o.getNom();
        this.description = o.getDescription();
        this.details = new Hyperlink("détails...");
        this.details.setOnMouseClicked(new ControleurAccesObjet(o.getId()));
    }

    public String getName()
    {
        return this.name;
    }

    public String getDescription() {
        return description;
    }

    public Hyperlink getDetails() {
        return details;
    }
}
