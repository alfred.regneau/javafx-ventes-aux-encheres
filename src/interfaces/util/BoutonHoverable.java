package interfaces.util;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;

public class BoutonHoverable extends Button
{
    private boolean selectionne;

    public BoutonHoverable(String texte)
    {
        super(texte);
        this.selectionne = false;
        this.setOnMouseEntered(new OnHoverHandler());
        this.setOnMouseExited(new OnUnhoverHandler());

        this.setBorder(Border.EMPTY);
        this.setBackground(Background.EMPTY);
        this.setCursor(Cursor.HAND);
    }

    public void setSelectionne(boolean selectionne)
    {
        if(selectionne)
        {
            this.setBackground(Styles.HOVER_BACKGROUND);
            this.selectionne = true;
        }
        else
        {
            this.setBackground(Background.EMPTY);
            this.selectionne = false;
        }
    }

    class OnHoverHandler implements EventHandler<MouseEvent>
    {
        @Override
        public void handle(MouseEvent mouseEvent)
        {
            ((BoutonHoverable)mouseEvent.getTarget()).setBackground(Styles.HOVER_BACKGROUND);

        }
    }

    class OnUnhoverHandler implements EventHandler<MouseEvent>
    {
        @Override
        public void handle(MouseEvent mouseEvent)
        {
            if(!selectionne)
                ((BoutonHoverable)mouseEvent.getTarget()).setBackground(Background.EMPTY);
        }
    }
}
