package interfaces.util;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class CadreImageDescriptionEditable extends CadreImageDescription {
    private ImageView edit;
    private int id;

    public static final Image EDIT_ICONE = new Image("images/icones/edit.png");

    public CadreImageDescriptionEditable(int id, Image image, double largeur, double hauteur, String titre, String description, int maxCharDescription, EventHandler<MouseEvent> actionClic)
    {
        super(image, largeur, hauteur, titre, description, maxCharDescription, "", actionClic);
        this.edit = new ImageView(EDIT_ICONE);
        this.conteneurPrix.getChildren().add(this.edit);
        this.id = id;
    }

    public int getIdCadre() {
        return id;
    }
}
