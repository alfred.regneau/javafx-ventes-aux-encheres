package interfaces.util;

import controleurs.ControleurAccesVente;
import exceptions.ResultatVideException;
import javafx.scene.control.Alert;
import modeles.Vente;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;

public class EntreeTableViewVente extends EntreeTableView
{
    private String prix;

    public EntreeTableViewVente(Vente v)
    {
        super(v.getObjet());
        try
        {
            this.prix = Requetes.recupererPropositionMaximumPourVente(GestionnaireSQL.getConnexion(), v.getId()).getMontant() + "€";
        }

        catch(SQLException e)
        {
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de récupérer la vente", "Une erreur est survenue pendant l'accès à la base de données.");
        }

        catch(ResultatVideException e)
        {
            this.prix = v.getPrixBase() + "€";
        }
        super.getDetails().setOnMouseClicked(new ControleurAccesVente(v));

    }

    public String getPrix()
    {
        return prix;
    }
}
