package interfaces.util;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;

public class CadreImageDescriptionAlternatif extends CadreImageDescription
{
    public CadreImageDescriptionAlternatif(Image image, double largeur, double hauteur, String titre, String description, int maxCharDescription, String monEnchere, String enchereMax, boolean enchMax, EventHandler<MouseEvent> actionClic)
    {
        // TODO afficher la couronne
        super(image, largeur, hauteur, titre, description, maxCharDescription, monEnchere, actionClic);
        Label lblMonEn = new Label("Mon enchère : " + monEnchere);
        Styles.ajouterStyle(lblMonEn, Styles.PARAGRAPHE);
        Label lblEnMax = new Label("\nEnchère la plus haute : " + enchereMax);
        lblEnMax.setWrapText(true);
        Styles.ajouterStyle(lblEnMax, Styles.ANNOTATION);
        this.conteneurPrix.getChildren().clear();
        this.conteneurPrix.getChildren().addAll(lblMonEn, lblEnMax);
    }
}
