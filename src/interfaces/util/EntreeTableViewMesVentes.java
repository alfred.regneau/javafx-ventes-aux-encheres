package interfaces.util;


import controleurs.ControleurTableViewMesVentes;
import interfaces.MesVentes;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import modeles.Statut;
import modeles.Vente;

public class EntreeTableViewMesVentes {

    private String name;
    private Hyperlink detail;
    private FlowPane nomEtDetails;
    private FlowPane statut;
    private FlowPane action;
    private FlowPane prix;
    private BoutonIcone valider;
    private BoutonIcone poubelle;
    private BoutonIcone editer;
    private MesVentes vue;
    private ControleurTableViewMesVentes controleur;
    public static final Image ICONE_POUBELLE = new Image("images/icones/trash.png");
    public static final Image ICONE_VALIDER = new Image("images/icones/valider.png");
    public static final Image ICONE_EDITER = new Image("images/icones/edit.png");

    public EntreeTableViewMesVentes(MesVentes vue, Vente v) {
        this.vue = vue;
        this.controleur = new ControleurTableViewMesVentes(v, vue);
        this.name = v.getObjet().getNom();
        this.detail = new Hyperlink("détail...");
        this.detail.setOnAction(controleur);


        this.nomEtDetails = new FlowPane();
        this.nomEtDetails.setAlignment(Pos.CENTER_LEFT);
        this.nomEtDetails.getChildren().addAll(new Label(name), detail);
        this.statut = new FlowPane(new Label(v.getStatut().getNomStatut()));
        this.statut.setAlignment(Pos.CENTER_LEFT);
        this.valider = new BoutonIcone(ICONE_VALIDER, 20, 20, controleur);
        this.poubelle = new BoutonIcone(ICONE_POUBELLE, 20, 20, controleur);
        this.editer = new BoutonIcone(ICONE_EDITER, 20, 20, controleur);
        this.action = new FlowPane();
        this.action.setAlignment(Pos.CENTER);
        int idStatut = v.getStatut().getIdStatut();
        if (idStatut == 1)
            this.action.getChildren().addAll(editer, poubelle);
        if(idStatut == 2)
            this.action.getChildren().add(poubelle);
        else if(idStatut == 3)
            this.action.getChildren().addAll(valider, poubelle);

        this.prix = new FlowPane(new Label(v.getPrixBase() + "€"));
        this.prix.setAlignment(Pos.CENTER_LEFT);
    }

    public Hyperlink getDetail() {
        return detail;
    }

    public FlowPane getAction() {
        return action;
    }

    public String getName() {
        return name;
    }

    public FlowPane getPrix() {
        return prix;
    }

    public FlowPane getStatut() {
        return statut;
    }

    public BoutonIcone getPoubelle() {
        return poubelle;
    }

    public FlowPane getNomEtDetails() {
        return nomEtDetails;
    }


}
