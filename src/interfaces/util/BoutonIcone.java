package interfaces.util;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class BoutonIcone extends BoutonHoverable
{
    private Image icone;

    public BoutonIcone(Image icone, double largeur, double hauteur, EventHandler<ActionEvent> actionHandler)
    {
        super("");
        this.setPrefSize(largeur, hauteur);
        this.setBorder(Border.EMPTY);
        this.setBackground(Background.EMPTY);
        this.icone = icone;

        ImageView imageView = new ImageView(this.icone);
        imageView.setFitHeight(hauteur);
        imageView.setFitWidth(largeur);
        imageView.setPreserveRatio(true);

        this.setGraphic(imageView);
        this.setGraphicTextGap(5);;
        this.setPadding(new Insets(5, 5, 5, 5));

        this.setOnAction(actionHandler);
    }

    public Image getIcone()
    {
        return this.icone;
    }
}
