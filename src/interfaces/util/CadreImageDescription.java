package interfaces.util;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;


public class CadreImageDescription extends FlowPane
{
    protected VBox conteneurPrix;

    // TODO afficher couronne
    public CadreImageDescription(Image image, double largeur, double hauteur, String titre, String description, int maxCharDescription, String prix, EventHandler<MouseEvent> actionClic)
    {
        super();

        double tailleZoneImage = Math.min(largeur, hauteur);
        boolean affichageVertical = largeur < hauteur;

        double largeurConteneurTexte = affichageVertical ? largeur : largeur - tailleZoneImage;
        double hauteurConteneurTexte = affichageVertical ? hauteur - tailleZoneImage : hauteur;

        if(description.length() > maxCharDescription)
            description = description.substring(0, maxCharDescription - 3) + "...";

        this.setPadding(new Insets(5));
        this.setPrefSize(largeur, hauteur);
        this.setOnMouseEntered(new OnHoverHandler());
        this.setOnMouseExited(new OnUnhoverHandler());
        this.setOnMouseClicked(actionClic);
        this.setCursor(Cursor.HAND);
        this.setBorder(Styles.BLACK_BORDER);
        this.setBackground(Styles.DEFAULT_BACKGROUND);

        AnchorPane conteneur = new AnchorPane();


        ImageCarreeCentree conteneurImage = new ImageCarreeCentree(image, tailleZoneImage);
        conteneurImage.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        AnchorPane.setLeftAnchor(conteneurImage, 0.0);


        AnchorPane conteneurTexte = new AnchorPane();
        AnchorPane.setRightAnchor(conteneurTexte, 0.0);
        conteneurTexte.setPrefSize(largeurConteneurTexte - (affichageVertical ? 0 : 20), hauteurConteneurTexte - (affichageVertical ? 20 : 0));
        conteneurTexte.setPadding(new Insets(5, 0, 5, 0));


        Label titreLbl = new Label(titre);
        titreLbl.setWrapText(true);
        titreLbl.setPrefWidth(conteneurTexte.getPrefWidth());
        Styles.ajouterStyle(titreLbl, Styles.TITRE_3);
        Styles.ajouterStyle(titreLbl, Styles.GRAS);

        Label descLbl = new Label(description);
        descLbl.setPrefWidth(largeurConteneurTexte);
        descLbl.setWrapText(true);
        Styles.ajouterStyle(descLbl, Styles.ANNOTATION);
        Styles.ajouterStyle(descLbl, Styles.PARAGRAPHE);

        VBox conteneurTitreDescription = new VBox();
        conteneurTitreDescription.setSpacing(10);
        conteneurTitreDescription.setPrefWidth(conteneurTexte.getPrefWidth());
        conteneurTitreDescription.getChildren().addAll(titreLbl, descLbl);
        AnchorPane.setTopAnchor(conteneurTitreDescription, 0.0);


        Label prixLbl = new Label(prix);
        prixLbl.setStyle(Styles.PRIX_1);

        conteneurPrix = new VBox();
        conteneurPrix.setPrefWidth(conteneurTexte.getPrefWidth());
        conteneurPrix.setAlignment(Pos.TOP_RIGHT);
        conteneurPrix.getChildren().add(prixLbl);
        AnchorPane.setBottomAnchor(conteneurPrix, 0.0);


        conteneurTexte.getChildren().addAll(conteneurTitreDescription, conteneurPrix);

        conteneur.setPrefSize(largeur - 10, hauteur - 10);
        conteneur.getChildren().addAll(conteneurImage, conteneurTexte);

        this.getChildren().add(conteneur);
    }

    class OnHoverHandler implements EventHandler<MouseEvent>
    {
        @Override
        public void handle(MouseEvent mouseEvent)
        {
            ((CadreImageDescription)mouseEvent.getTarget()).setBackground(Styles.HOVER_BACKGROUND);
        }
    }

    class OnUnhoverHandler implements  EventHandler<MouseEvent>
    {
        @Override
        public void handle(MouseEvent mouseEvent)
        {
            ((CadreImageDescription)mouseEvent.getTarget()).setBackground(Styles.DEFAULT_BACKGROUND);
        }
    }
}
