package interfaces.util;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

import java.util.Arrays;


public class BarreActions extends HBox
{
    private Node[] composants;

    public BarreActions(String titre, Node... composants)
    {
        this.composants = composants;
        this.genererInterface(titre);
    }

    private void genererInterface(String titre)
    {
        Label titreLbl = new Label(titre);
        titreLbl.setAlignment(Pos.CENTER_LEFT);
        Styles.ajouterStyle(titreLbl, Styles.TITRE_1);

        HBox actions = new HBox();
        actions.getChildren().addAll(Arrays.asList(composants));
        actions.setSpacing(10);
        actions.setAlignment(Pos.CENTER_RIGHT);
        actions.setPadding(new Insets(5, 5, 5, 0));
        HBox.setHgrow(actions, Priority.ALWAYS);
        this.getChildren().addAll(titreLbl, actions);
        this.setBorder(Styles.GRAY_BOTTOM_BORDER);
        this.setAlignment(Pos.CENTER_LEFT);
    }
}
