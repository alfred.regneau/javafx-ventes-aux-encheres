package interfaces.util;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;

import java.util.Optional;

public class Util
{
    public static Optional<ButtonType> alert(Alert.AlertType type, String title, String headerText, String descText)
    {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(headerText);

        Label text = new Label(descText);
        text.setWrapText(true);
        alert.getDialogPane().setContent(text);

        return alert.showAndWait();
    }
}
