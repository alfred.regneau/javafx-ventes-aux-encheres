package interfaces.util;

import controleurs.ControleurEntreeListeConversations;
import controleurs.GestionnairesFenetres;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import modeles.Conversation;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Date;

public class EntreeListeConversations extends HBox
{
    private Label correspondantLbl,
                  objetLbl,
                  dateLbl;

    private Conversation conv;

    public EntreeListeConversations(Conversation conv)
    {
        this.conv = conv;

        String correspondant = conv.getCorrespondant().getPseudo();
        String nomObjet = conv.getObjetConcerne().getNom();
        String date = "";

        LocalTime minuit = LocalTime.MIDNIGHT;
        LocalDate ajd = LocalDate.now();
        LocalDateTime ajdMinuit = LocalDateTime.of(ajd, minuit);

        if(conv.getDateDernierMessage().toInstant().isBefore(ajdMinuit.toInstant(ZoneOffset.UTC)))
            date = new SimpleDateFormat("dd/MM/yyyy").format(conv.getDateDernierMessage());
        else
            date = new SimpleDateFormat("HH:mm").format(conv.getDateDernierMessage());


        this.setPadding(new Insets(10, 15, 10, 15));
        this.setSpacing(5);
        Styles.ajouterStyle(this, Styles.TITRE_3);

        this.correspondantLbl = new Label(correspondant);
        this.correspondantLbl.setMaxWidth(150);
        this.correspondantLbl.setPrefWidth(150);
        Styles.ajouterStyle(this.correspondantLbl, Styles.GRAS);

        this.objetLbl = new Label(nomObjet);
        Styles.ajouterStyle(this.objetLbl, Styles.ITALIQUE);


        this.dateLbl = new Label(date);
        HBox dateConteneur = new HBox();
        dateConteneur.getChildren().add(this.dateLbl);
        dateConteneur.setAlignment(Pos.CENTER_RIGHT);
        HBox.setHgrow(dateConteneur, Priority.ALWAYS);

        this.getChildren().addAll(this.correspondantLbl, this.objetLbl, dateConteneur);
        this.setBorder(Styles.LIGHTGRAY_BORDER);
        this.setCursor(Cursor.HAND);

        ControleurEntreeListeConversations controleur = new ControleurEntreeListeConversations(this);
        this.setOnMouseClicked(controleur);
        this.setOnMouseEntered(controleur);
        this.setOnMouseExited(controleur);
    }

    public Conversation getConversation()
    {
        return this.conv;
    }
}
