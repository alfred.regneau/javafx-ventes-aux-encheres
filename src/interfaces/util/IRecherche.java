package interfaces.util;

import javafx.scene.control.TextField;

public interface IRecherche
{
    void recherche(String recherche);
    TextField getSearch();
}
