package interfaces.util;

import controleurs.ControleurEcranMultipages;
import interfaces.InterfaceMultipages;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;

public class BarreMultipages extends HBox {
    private int numPage;
    private ControleurEcranMultipages control;
    private int nbPages;

    public static final Image ICONE_GAUCHE = new Image("images/icones/gauche.png");
    public static final Image ICONE_DROITE = new Image("images/icones/droite.png");

    public BarreMultipages(int numPage, int nbPages, InterfaceMultipages page)
    {
        super();
        this.numPage = numPage;
        this.control = new ControleurEcranMultipages(page);
        this.nbPages = nbPages;
        this.genererInterface();
    }

    private void genererInterface()
    {
        this.setAlignment(Pos.CENTER);
        this.setBorder(Styles.LIGHTGRAY_TOP_BORDER);
        this.setPrefHeight(41);
        if(this.numPage > 1)
            this.getChildren().add(new BoutonIcone(ICONE_GAUCHE, 25, 25, this.control));
        this.getChildren().addAll(new Label(Integer.toString(this.numPage)), new Label(" / " + nbPages));
        if(this.numPage < nbPages)
            this.getChildren().add(new BoutonIcone(ICONE_DROITE, 25, 25, this.control));
    }

    public void majAffichage(int numPage, int nbPages)
    {
        this.numPage = numPage;
        this.nbPages = nbPages;
        this.getChildren().clear();
        if(this.numPage > 1)
            this.getChildren().add(new BoutonIcone(ICONE_GAUCHE, 25, 25, this.control));
        this.getChildren().addAll(new Label(this.numPage + " / " + nbPages));
        if(this.numPage < nbPages)
            this.getChildren().add(new BoutonIcone(ICONE_DROITE, 25, 25, this.control));
    }
}
