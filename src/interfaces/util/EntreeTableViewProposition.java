package interfaces.util;

import controleurs.ControleurAccesVente;
import modeles.Proposition;

public class EntreeTableViewProposition extends EntreeTableView {
    private String enchere;

    public EntreeTableViewProposition(Proposition p)
    {
        super(p.getVente().getObjet());
        this.enchere = p.getMontant() + "€";
        super.getDetails().setOnMouseClicked(new ControleurAccesVente(p.getVente()));
    }

    public String getEnchere() {
        return enchere;
    }
}
