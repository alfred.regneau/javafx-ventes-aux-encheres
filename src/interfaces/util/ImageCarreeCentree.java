package interfaces.util;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;

public class ImageCarreeCentree extends FlowPane
{
    private ImageView imageView;

    public ImageCarreeCentree(Image image, double taille)
    {
        super();

        imageView = new ImageView(image);
        imageView.setFitHeight(taille);
        imageView.setFitWidth(taille);
        imageView.setPreserveRatio(true);

        this.setPrefSize(taille, taille);
        this.setAlignment(Pos.CENTER);
        this.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        this.getChildren().add(imageView);
    }

    public void setImage(Image image)
    {
        this.imageView.setImage(image);
    }
}
