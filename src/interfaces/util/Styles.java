package interfaces.util;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class Styles {

    /**
     * le style des titres de niveau 1
     */
    public final static String TITRE_1 =    "-fx-font-size: 30px;" +
                                            "-fx-text-fill: black;" +
                                            "-fx-padding: 0 0 0 20;";
    /**
     * le style des titres de niveaux 2
     */
    public final static String TITRE_2 = 	"-fx-font-size: 20px;" +
                                            "-fx-text-fill: black;";
    /**
     * le style des titres de niveaux 3
     */
    public final static String TITRE_3 =    "-fx-font-size: 16px;" +
                                            "-fx-text-fill: black;";
    /**
     * le style du texte de base
     */
    public final static String PARAGRAPHE = "-fx-font-size: 14px;" +
                                            "-fx-text-fill: black;";

    /**
     * le style du texte des prix d'importance de niveau 1
     */
    public final static String PRIX_1 =     "-fx-font-size: 16px;" +
                                            "-fx-text-fill: red;" +
                                            "-fx-font-weight: bold;";

    /**
     * le style du texte des prix d'importance de niveau 2
     */
    public final static String PRIX_2 =     "-fx-font-size: 12px;" +
                                            "-fx-text-fill: black;" +
                                            "-fx-font-weight: bold;";

    /**
     * le style de textes peu important
     */
    public final static String ANNOTATION = "-fx-font-size: 12px;" +
                                            "-fx-font-style: italic;" +
                                            "-fx-text-fill: black;";

    public static final String ERREUR_ANNOTATION = "-fx-font-size: 12px; " +
            "                                       -fx-text-fill: red;";

    public static final String MESSAGERIE_DATE = "-fx-font-size: 12px;" +
                                                 "-fx-text-fill: gray;" +
                                                 "-fx-font-style: italic;";

    /**
     * le style d'un type de boutons
     */
    public final static String BTN =        "-fx-font-size: 16px;" +
                                            "-fx-font-weight: bold;" +
                                            "-fx-text-fill: black;" +
                                            "-fx-background-color: red;";

    public static final String GRAS = "-fx-font-weight: bold;";
    public static final String ITALIQUE = "-fx-font-style: italic;";

    public static final String TEXTE_BOUTON = "-fx-font-weight: bold; -fx-font-size: 14px;";
    public static final String TEXTE_BOUTON_NAVBAR = "-fx-font-size: 13px;";

    public static final String SCROLLPANE_BACKGROUND = "-fx-background: transparent; -fx-background-color: transparent;";

    public static final Background HOVER_BACKGROUND =  backgroundColor(Color.color(0.2, 0.2, 0.2, 0.1));
    public static final Background NAVBAR_BACKGROUND = backgroundColor(Color.WHITE);
    public static final Background DEFAULT_BACKGROUND = backgroundColor(Color.WHITE);

    public static final Border BLACK_BORDER = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT));
    public static final Border LIGHTGRAY_BORDER = new Border(new BorderStroke(Color.LIGHTGRAY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT));

    public static final Border GRAY_BOTTOM_BORDER = new Border(new BorderStroke(
            Color.grayRgb(204), Color.grayRgb(204), Color.grayRgb(204), Color.grayRgb(204),
            BorderStrokeStyle.NONE, BorderStrokeStyle.NONE, BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE,
            CornerRadii.EMPTY, new BorderWidths(1), Insets.EMPTY));

    public static final Border BLACK_BOTTOM_BORDER = new Border(new BorderStroke(
            Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK,
            BorderStrokeStyle.NONE, BorderStrokeStyle.NONE, BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE,
            CornerRadii.EMPTY, new BorderWidths(2), Insets.EMPTY));

    public static final Border LIGHTGRAY_TOP_BORDER = new Border(new BorderStroke(
            Color.LIGHTGRAY, Color.LIGHTGRAY, Color.LIGHTGRAY, Color.LIGHTGRAY,
            BorderStrokeStyle.SOLID, BorderStrokeStyle.NONE, BorderStrokeStyle.NONE, BorderStrokeStyle.NONE,
            CornerRadii.EMPTY, new BorderWidths(1), Insets.EMPTY));

    public static void ajouterStyle(Node node, String style)
    {
        node.setStyle(node.getStyle() + style);
    }

    public static void ecraserStyle(Node node, String style)
    {
        node.setStyle(style);
    }

    public static Background backgroundColor(Color couleur)
    {
        return new Background(new BackgroundFill(couleur, CornerRadii.EMPTY, Insets.EMPTY));
    }
}
