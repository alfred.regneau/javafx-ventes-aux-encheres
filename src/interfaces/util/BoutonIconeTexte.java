package interfaces.util;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;

public class BoutonIconeTexte extends BoutonIcone
{
    public BoutonIconeTexte(Image icone, String texte, double hauteur, EventHandler<ActionEvent> actionHandler)
    {
        super(icone, -1, hauteur, actionHandler);
        this.setText(texte);
        Styles.ajouterStyle(this, Styles.TEXTE_BOUTON);
    }
}