package interfaces;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import controleurs.ControleurAccesObjet;
import controleurs.ControleurCreationObjet;
import controleurs.ControleurLigneQuadrillage;
import controleurs.ControleurRecherche;
import interfaces.util.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import modeles.ConnexionUtilisateur;
import modeles.Objet;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MesAcquisitions extends InterfaceMultipages  implements IChangeMode, IRecherche {
    private BoutonIcone quadrillage;
    private BoutonIcone line;
    private TextField search;
    private BoutonIconeTexte addElement;
    private List<Objet> objets;
    private List<CadreImageDescriptionEditable> cadres;
    private boolean classic;
    private ScrollPane sp;

    public MesAcquisitions()
    {
        super(0);

        try
        {
            this.objets = Requetes.recupererObjetsUtilisateur(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de récupérer vos objets", "Une erreur a eu lieu lors de la récupération de vos acquisitions dans la base de données. N'ayez crainte, cette erreur n'a pas engendré de pertes de données.");
        }

        ControleurLigneQuadrillage controlAffichage = new ControleurLigneQuadrillage(this);

        this.menuNavigation.selectionner(MenuNavigation.ICONE_MESACQUISITIONS);
        this.classic = true;
        this.quadrillage = new BoutonIcone(InterfaceModeConnecte.ICONE_QUADRILLAGE, 25, 25, controlAffichage);
        this.line = new BoutonIcone(InterfaceModeConnecte.ICONE_LINE, 25, 25, controlAffichage);
        this.addElement = new BoutonIconeTexte(new Image("images/icones/plus.png"), "Ajouter un élément", 40, new ControleurCreationObjet());
        this.search = new TextField();
        this.search.setOnAction(new ControleurRecherche(this));
        this.search.setPromptText("Rechercher...");
        this.cadres = new ArrayList<>();
        this.sp = new ScrollPane();

        super.setNbObjets(this.objets.size());
    }

    @Override
    public TextField getSearch()
    {
        return this.search;
    }

    @Override
    protected Node getContentPane() {
        try
        {
            this.objets = Requetes.recupererObjetsUtilisateur(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte());
            super.setNbObjets(this.objets.size());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de récupérer vos objets", "Une erreur a eu lieu lors de la récupération de vos acquisitions dans la base de données. N'ayez crainte, cette erreur n'a pas engendré de pertes de données.");
        }

        BorderPane main = new BorderPane();
        BarreActions ba = new BarreActions("Mes acquisitions", this.quadrillage, this.line, this.search);

        VBox center = new VBox(20);
        center.setPrefWidth(780);
        center.setPrefHeight(456);
        center.setPadding(new Insets(20, 0, 0, 20));
        if(super.changePage)
        {
            this.sp.setVvalue(0);
            super.changePage = false;
        }
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        Styles.ajouterStyle(sp, Styles.SCROLLPANE_BACKGROUND);
        sp.setContent(this.classic ? this.constructClassic() : this.constructPro());

        center.getChildren().addAll(this.addElement, sp);

        main.setTop(ba);
        main.setCenter(center);

        return main;
    }

    private GridPane constructClassic()
    {
        GridPane content = new GridPane();

        content.setAlignment(Pos.CENTER);
        content.setHgap(20);
        content.setVgap(content.getHgap());
        content.add(this.addElement, 0, 0);

        this.cadres.clear();
        for(int i = (this.numPage - 1) * this.nbObjetsParPage ; i < (this.numPage - 1) * this.nbObjetsParPage + super.nbObjetsAffichables() ; i++)
        {
            try
            {
                this.cadres.add(new CadreImageDescriptionEditable(this.objets.get(i).getId(), new Image(new ByteArrayInputStream(this.objets.get(i).getPhotos().get(0).getImg())), 350, 150, this.objets.get(i).getNom(), this.objets.get(i).getDescription(), 80, new ControleurAccesObjet(this.objets.get(i).getId())));
            }

            catch(IndexOutOfBoundsException e)
            {
                System.out.println("loser");
            }
        }
        int y = 0;
        for(int i = 0; i < this.cadres.size(); i++)
        {
            if(i % 2 == 0)
                y++;

            content.add(this.cadres.get(i), i % 2, y);
        }

        return content;
    }

    private TableView<EntreeTableView> constructPro()
    {
        TableView<EntreeTableView> tv = new TableView<>();
        tv.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tv.setPrefHeight(350);
        tv.setMinWidth(750);
        tv.setBackground(Styles.backgroundColor(Color.LIGHTGRAY));

        tv.getColumns().addAll(new TableColumn<EntreeTableView, String>("Nom"), new TableColumn<EntreeTableView, String>("Description"), new TableColumn<EntreeTableView, Hyperlink>(""));
        tv.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        tv.getColumns().get(0).prefWidthProperty().bind(tv.widthProperty().multiply(1/3.0));
        tv.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("description"));
        tv.getColumns().get(1).prefWidthProperty().bind(tv.widthProperty().multiply(1/3.0));
        tv.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("details"));
        tv.getColumns().get(2).prefWidthProperty().bind(tv.widthProperty().multiply(1/3.0));
        tv.getColumns().get(2).setStyle(tv.getColumns().get(2).getStyle() +  "-fx-alignment: CENTER-RIGHT;");

        tv.setItems(getObjectsList());

        // PERMET D'EMPECHER DE REORDONNER LES COLONNES DE LA TABLEVIEW
        tv.widthProperty().addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth)
            {
                TableHeaderRow header = (TableHeaderRow) tv.lookup("TableHeaderRow");
                header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                        header.setReordering(false);
                    }
                });
            }
        });

        return tv;
    }

    private ObservableList<EntreeTableView> getObjectsList()
    {
        List<EntreeTableView> list = new ArrayList<>();
        for(Objet o : this.objets)
            list.add(new EntreeTableView(o));

        return FXCollections.observableArrayList(list);
    }

    @Override
    public void changeAffichage(boolean valeur)
    {
        this.classic = valeur;
        this.getInterface();
    }

    @Override
    public void recherche(String recherche)
    {
        try
        {
            this.objets = Requetes.rechercheObjetUtilisateur(GestionnaireSQL.getConnexion(), recherche, ConnexionUtilisateur.getIdUtilisateurConnecte());
            super.setNbObjets(this.objets.size());
        }

        catch(SQLException e)
        {
            Util.alert(Alert.AlertType.ERROR, "", "", "");
        }

        this.getInterface();
    }
}
