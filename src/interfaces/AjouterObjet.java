package interfaces;

import controleurs.ControleurEditObjet;
import controleurs.ControleurEditObjetImage;
import controleurs.ControleurEditObjetSupprImage;
import controleurs.GestionnairesFenetres;
import exceptions.IntrouvableException;
import interfaces.util.*;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import modeles.Categorie;
import modeles.Objet;
import modeles.Photo;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AjouterObjet extends InterfaceModeConnecte {

    private BorderPane main;
    private BoutonIconeTexte save;
    private BoutonIconeTexte back;
    private TextField name;
    private TextArea desc;
    private BoutonIconeTexte addImage;
    private List<HBox> photos;
    private Objet objet;
    private ControleurEditObjet control;
    private boolean newObjet;
    private List<Integer> idsSuppr;
    private ComboBox<Categorie> categories;

    public AjouterObjet(int id)
    {
        super();
        this.main = new BorderPane();
        this.control = new ControleurEditObjet(this);
        this.categories = new ComboBox<>();
        super.stop();

        try {
            this.categories.getItems().addAll(Requetes.recupererCategories(GestionnaireSQL.getConnexion()));
            if(id != -1) {
                this.newObjet = false;
                this.objet = Requetes.recupererObjetParId(GestionnaireSQL.getConnexion(), id);
                this.categories.getSelectionModel().select(this.objet.getCategorie());
            }

            else {
                this.newObjet = true;
                this.objet = new Objet(Requetes.maxIdentifiantObjet(GestionnaireSQL.getConnexion()) + 1, "", "", null);
            }
        }

        catch(SQLException e)
        {
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible d'accéder à la base de données.", "Une erreur s'est produite pendant l'accès à la base de données.");
        }

        catch (IntrouvableException e)
        {
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : Objet introuvable", "L'objet sélectionné est introuvable dans la base de données.");
        }

        this.save = new BoutonIconeTexte(ICONE_SAUVEGARDE, "Enregistrer", 40, this.control);
        this.back = new BoutonIconeTexte(ICONE_RETOUR, "Retour", 40, this.control);
        this.name = new TextField(this.objet.getNom());
        this.name.setPromptText("Nom de l'objet :");
        this.desc = new TextArea(this.objet.getDescription());
        this.desc.setWrapText(true);
        this.desc.setPromptText("Description :");
        this.desc.setPrefWidth(400);
        this.desc.setPrefHeight(365);
        int size = 300;
        this.name.setPrefWidth(size);
        this.name.setMinWidth(size);
        this.name.setMaxWidth(size);
        this.addImage = new BoutonIconeTexte(ICONE_AJOUTER, "Ajouter une image", 40, this.control);
        this.photos = new ArrayList<>();
        this.idsSuppr = new ArrayList<>();
    }

    @Override
    protected Node getContentPane() {
        this.photos.clear();
        BarreActions ba = new BarreActions("Editer un objet", this.save, this.back);

        AnchorPane center = new AnchorPane();

        VBox left = new VBox(15);
        left.getChildren().addAll(this.name, this.categories, this.desc);

        VBox right = new VBox(20);
        right.setAlignment(Pos.CENTER_RIGHT);
        right.getChildren().add(addImage);
        for(int i = 0; i < this.objet.getPhotos().size(); i++)
        {
            this.photos.add(new HBox(15));
            HBox title = new HBox(70);
            title.getChildren().addAll(new Label("Photo " + (i+1)), new BoutonIcone(ICONE_SUPPRIMER, 25, 25, new ControleurEditObjetSupprImage(i, this)));
            ImageView img = new ImageView(new Image(new ByteArrayInputStream(this.objet.getPhotos().get(i).getImg())));
            img.setFitWidth(65);
            img.setPreserveRatio(true);
            Hyperlink importe = new Hyperlink("Cliquez pour importer");
            importe.setOnAction(new ControleurEditObjetImage(i, this));
            VBox rightImg = new VBox(15);
            rightImg.getChildren().addAll(title, importe);
            this.photos.get(i).getChildren().addAll(img, rightImg);
            right.getChildren().add(this.photos.get(i));
        }

        if(this.objet.getPhotos().size() >= 4)
            this.addImage.setDisable(true);
        else
            this.addImage.setDisable(false);

        AnchorPane.setLeftAnchor(left, 30.0);
        AnchorPane.setRightAnchor(right, 30.0);
        AnchorPane.setTopAnchor(left, 30.0);
        AnchorPane.setTopAnchor(right, 60.0);
        center.getChildren().addAll(left, right);

        main.setTop(ba);
        main.setCenter(center);

        return main;
    }

    public void sauvegarde()
    {
        this.objet.setNom(this.name.getText());
        this.objet.setDescription(this.desc.getText());
        this.objet.setCtgr(this.categories.getValue());
        if(!this.objet.getNom().equals("") && !this.categories.getSelectionModel().isEmpty() && this.objet.getPhotos().size() > 0) {
            try {
                if (!this.newObjet)
                    Requetes.modifierObjet(GestionnaireSQL.getConnexion(), this.objet, this.idsSuppr);
                else
                    Requetes.ajouterObjet(GestionnaireSQL.getConnexion(), this.objet);
            } catch (SQLException e) {
                Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de sauvegarder", "Une erreur s'est produite lors de la sauvegarde dans la base de données.");
            }
        }

        else
        {
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : Un champ n'est pas rempli", "Tous les champs obligatoires doivent être rempli pour pouvoir sauvegarder l'objet.");
        }
    }

    public void changeImage(int id, File file)
    {
        byte[] bytesArray = new byte[(int)file.length()];

        try {
            FileInputStream fis = new FileInputStream(file);

            try {
                fis.read(bytesArray);
                fis.close();
            }

            catch(IOException e)
            {
                Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur", "Erreur");
            }
        }
        catch(FileNotFoundException e)
        {
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : Fichier introuvable", "Une erreur est survenue lors de l'ouverture du fichier.");
        }

        this.objet.getPhotos().get(id).setImg(bytesArray);
        this.photos.clear();
        super.getInterface();
    }

    public void ajouterImage()
    {
        try {
            this.objet.ajouterPhoto(new Photo(Requetes.maxIdentifiantPhoto(GestionnaireSQL.getConnexion()) + 1, "titre", new byte[0]));
            this.photos.clear();
            super.getInterface();
        }

        catch(SQLException e)
        {
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible d'ajouter l'image", "Une erreur s'est produite lors de l'ajout de l'image.");
        }
    }

    public void supprimerImage(int i)
    {
        this.idsSuppr.add(this.objet.getPhotos().get(i).getId());
        this.objet.supprimerPhoto(i);
        this.photos.clear();
        super.getInterface();
    }
}
