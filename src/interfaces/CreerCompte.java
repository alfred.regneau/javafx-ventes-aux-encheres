package interfaces;

import controleurs.ControleurInscription;
import controleurs.ControleurPseudoTextfield;
import controleurs.GestionnairesFenetres;
import controleurs.ListenerPseudoTextField;
import interfaces.util.Styles;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import static javafx.geometry.Orientation.HORIZONTAL;

public class CreerCompte extends VBox
{

    private TextField tfPseudo;
    private TextField tfMail;
    private PasswordField tfMdp;
    private PasswordField tfConfirmMdp;

    private Label pseudoSublabel;
    private Label emailSublabel;
    private Label mdpSublabel;
    private Label confirmMdpSublabel;

    public CreerCompte()
    {
        super(10);
        sceneInscription();
        this.setAlignment(Pos.CENTER);
    }

    private void sceneInscription()
    {
        ControleurInscription controleurInscription = new ControleurInscription(this);

        Label titre = new Label("Inscription");
        Styles.ajouterStyle(titre, Styles.TITRE_1);

        VBox formulaire = formulaire(controleurInscription);

        Button sinscrire = new Button("S'inscrire");
        sinscrire.setPadding(new Insets(10, 20, 10, 20));
        sinscrire.setOnAction(controleurInscription);

        Separator separateur = new Separator(HORIZONTAL);
        separateur.setMaxWidth(600);

        Label lDejaUnCompte = new Label("Vous avez déjà un compte ? Connectez-vous ");
        Hyperlink lien = new Hyperlink("ici");
        lien.setOnAction(controleurInscription);

        HBox dejaUnCompte = new HBox();
        dejaUnCompte.setAlignment(Pos.BASELINE_CENTER);
        dejaUnCompte.getChildren().addAll(lDejaUnCompte, lien);
        Styles.ajouterStyle(dejaUnCompte, Styles.ANNOTATION);

        this.setAlignment(Pos.CENTER);
        this.getChildren().addAll(titre, formulaire, sinscrire, separateur, dejaUnCompte);
    }

    private VBox formulaire(EventHandler<ActionEvent> controleur)
    {
        Label lID = new Label("Identifiant");
        Styles.ajouterStyle(lID, Styles.TITRE_2);
        this.tfPseudo = new TextField();
        this.tfPseudo.setOnAction(controleur);
        this.tfPseudo.textProperty().addListener(new ListenerPseudoTextField(this));

        this.pseudoSublabel = new Label("");
        Styles.ajouterStyle(this.pseudoSublabel, Styles.ERREUR_ANNOTATION);

        Label lMail = new Label("Adresse Email");
        Styles.ajouterStyle(lMail, Styles.TITRE_2);
        this.tfMail = new TextField();
        this.tfMail.setOnAction(controleur);

        this.emailSublabel = new Label("");
        Styles.ajouterStyle(this.emailSublabel, Styles.ERREUR_ANNOTATION);

        Label lMdp = new Label("Mot de passe");
        Styles.ajouterStyle(lMdp, Styles.TITRE_2);
        this.tfMdp = new PasswordField();
        this.tfMdp.setOnAction(controleur);

        this.mdpSublabel = new Label("");
        Styles.ajouterStyle(this.mdpSublabel, Styles.ERREUR_ANNOTATION);

        Label lConfirmMdp = new Label("Confirmation mot de passe :");
        Styles.ajouterStyle(lConfirmMdp, Styles.TITRE_2);
        this.tfConfirmMdp = new PasswordField();
        this.tfConfirmMdp.setOnAction(controleur);

        this.confirmMdpSublabel = new Label("");
        Styles.ajouterStyle(this.confirmMdpSublabel, Styles.ERREUR_ANNOTATION);

        VBox formulaire = new VBox(10);
        formulaire.getChildren().addAll(lID, tfPseudo, pseudoSublabel, lMail, tfMail, emailSublabel, lMdp, tfMdp, mdpSublabel, lConfirmMdp, tfConfirmMdp, confirmMdpSublabel);
        formulaire.setAlignment(Pos.CENTER);
        formulaire.setMaxWidth(400);
        formulaire.setPadding(new Insets(20, 25, 10, 25));
        formulaire.setBorder(Styles.LIGHTGRAY_BORDER);

        return formulaire;
    }

    public String getMdp()
    {
        return this.tfMdp.getText();
    }

    public String getConfirmMdp()
    {
        return this.tfConfirmMdp.getText();
    }

    public String getPseudo()
    {
        return this.tfPseudo.getText();
    }

    public String getEmail()
    {
        return this.tfMail.getText();
    }

    public void identifiantDejaPris()
    {
        this.pseudoSublabel.setText("Pseudo déjà pris !");
    }

    public void identifiantVide()
    {
        this.pseudoSublabel.setText("Veuillez renseigner un identifiant");
    }

    public void emailDejaPris()
    {
        this.emailSublabel.setText("Email déjà utilisé !");
    }

    public void motDePasseVide()
    {
        this.mdpSublabel.setText("Veuillez rentrer un mot de passe");
    }

    public void confirmationNeCorrespondPas()
    {
        this.confirmMdpSublabel.setText("Les deux mots de passe ne correspondent pas !");
    }

    public void reinitSublabels()
    {
        this.pseudoSublabel.setText("");
        this.emailSublabel.setText("");
        this.mdpSublabel.setText("");
        this.confirmMdpSublabel.setText("");
    }

    public void verifiePseudo() {
        if (tfPseudo.getText().length() > 20){
            tfPseudo.replaceText(20, tfPseudo.getText().length(), "");
            this.pseudoSublabel.setText("20 caractères maximum");
        }
        else
            this.pseudoSublabel.setText("");
    }
}
