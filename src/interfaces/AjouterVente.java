package interfaces;

import controleurs.ControleurAjouterVente;
import interfaces.util.BarreActions;
import interfaces.util.BoutonIconeTexte;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import modeles.ConnexionUtilisateur;
import modeles.Objet;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AjouterVente extends InterfaceModeConnecte
{


    private DatePicker dateDeb;
    private DatePicker dateFin;

    private TextField heureDeb;
    private TextField heureFin;
    private TextField MaPTF;
    private TextField ObjP;

    private ComboBox<Objet> allObj;

    private void setHaut(VBox V)
    {
        ControleurAjouterVente control = new ControleurAjouterVente(this);
        BoutonIconeTexte btnRetour = new BoutonIconeTexte(InterfaceModeConnecte.ICONE_RETOUR, "Retour", 40, control);
        BoutonIconeTexte btnSave = new BoutonIconeTexte(InterfaceModeConnecte.ICONE_SEND, "Enregistrer", 40, control);
        super.stop();

        BarreActions barreActions = new BarreActions("Éditer une vente", btnSave, btnRetour);

        V.getChildren().add(barreActions);

    }

    private void setCentre(GridPane g)
    {
        Label cObjet = new Label("Veuillez choisir un objet :");
        Label jDeb = new Label("Jour et heure de début");
        Label jfin = new Label("Jour et heure de Fin");
        Label MaP = new Label("Mise à prix");
        Label ObjL = new Label("Prix de réserve");
        Label WarnNumber = new Label("Exemple : 100.00.");
        Label WarnNumber2 = new Label("Le symbole '€' ne doit pas apparaître.");

        List<Objet> objDispo = new ArrayList<>();

        try
        {
            objDispo = Requetes.recupererObjetsNonEnVente(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        this.allObj = new ComboBox<>();
        this.allObj.getValue();
        this.allObj.setItems(FXCollections.observableList(objDispo));
        this.dateDeb = new DatePicker();
        dateDeb.setPromptText("JJ/MM/AAAA");
        this.heureDeb = new TextField();
        heureDeb.setPromptText("HH:MM");
        this.dateFin = new DatePicker();
        dateFin.setPromptText("JJ/MM/AAAA");
        this.heureFin = new TextField();
        heureFin.setPromptText("HH:MM");
        this.MaPTF = new TextField();
        MaPTF.setPromptText("0,00 €");
        this.ObjP = new TextField();
        ObjP.setPromptText("0,00 €");

        g.addColumn(1, cObjet, jDeb , jfin, MaP, ObjL);
        g.addColumn(2, allObj, dateDeb, dateFin, MaPTF, ObjP);
        g.add(heureDeb,3,1);
        g.add(heureFin,3,2);
        g.add(WarnNumber,3,3);
        g.add(WarnNumber2,3,4);

        g.setVgap(30.);
        g.setHgap(20.);
        g.setPadding(new Insets(40, 0, 0, 100));

    }

    public AjouterVente(int id)
    {
        super();
    }

    @Override
    protected BorderPane getContentPane()
    {
        BorderPane center = new BorderPane();


        VBox V = new VBox();
        setHaut(V);
        center.setTop(V);

        GridPane g = new GridPane();
        setCentre(g);
        center.setCenter(g);

        return center;
    }

    public DatePicker getDateDeb()
    {
        return dateDeb;
    }

    public void setDateDeb(DatePicker dateDeb)
    {
        this.dateDeb = dateDeb;
    }

    public DatePicker getDateFin()
    {
        return dateFin;
    }

    public void setDateFin(DatePicker dateFin)
    {
        this.dateFin = dateFin;
    }

    public TextField getHeureDeb()
    {
        return heureDeb;
    }

    public void setHeureDeb(TextField heureDeb)
    {
        this.heureDeb = heureDeb;
    }

    public TextField getHeureFin()
    {
        return heureFin;
    }

    public void setHeureFin(TextField heureFin)
    {
        this.heureFin = heureFin;
    }

    public TextField getMaPTF()
    {
        return MaPTF;
    }

    public void setMaPTF(TextField maPTF)
    {
        MaPTF = maPTF;
    }

    public ComboBox<Objet> getAllObj()
    {
        return allObj;
    }

    public void setAllObj(ComboBox<Objet> allObj)
    {
        this.allObj = allObj;
    }

    public TextField getObjP()
    {
        return ObjP;
    }

    public void setObjP(TextField objP)
    {
        ObjP = objP;
    }
}
