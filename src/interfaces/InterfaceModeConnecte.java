package interfaces;

import controleurs.ControleurMAJ;
import interfaces.util.Styles;
import interfaces.util.Util;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.util.Duration;
import modeles.ConnexionUtilisateur;

public abstract class InterfaceModeConnecte extends BorderPane
{
    protected MenuNavigation menuNavigation;
    private Timeline tl;
    public static final Image ICONE_QUADRILLAGE = new Image("images/icones/quadrillage.png");
    public static final Image ICONE_LINE = new Image("images/icones/ligne.png");
    public static final Image ICONE_SEND = new Image("images/icones/send.png");
    public final static Image ICONE_SAUVEGARDE = new Image("images/icones/save.png");
    public final static Image ICONE_RETOUR = new Image("images/icones/back.png");
    public final static Image ICONE_AJOUTER = new Image("images/icones/plus.png");
    public final static Image ICONE_SUPPRIMER = new Image("images/icones/close.png");

    public InterfaceModeConnecte()
    {
        super();

        if(!ConnexionUtilisateur.estConnecte())
        {
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : vous n'êtes pas connecté", "Vous vous trouvez sur une page nécessitant une authentification sans être connecté. L'application va se fermer.");
            System.exit(1);
        }


        menuNavigation = new MenuNavigation();
        this.setTop(menuNavigation);
        this.setBackground(Styles.DEFAULT_BACKGROUND);
        this.tl = new Timeline(new KeyFrame(Duration.seconds(1), new ControleurMAJ(this)));
        this.tl.setCycleCount(Timeline.INDEFINITE);
        this.tl.play();
    }

    protected abstract Node getContentPane();

    /**
     * Appelé pour récupérer l'interface
     * @return
     */
    public BorderPane getInterface()
    {
        // POUR QUE LES ATTRIBUTS SOIENT INITIALISÉS
        this.setCenter(getContentPane());
        return this;
    }

    public void stop()
    {
        this.tl.stop();
    }
}
