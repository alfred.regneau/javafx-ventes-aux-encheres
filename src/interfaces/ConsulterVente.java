package interfaces;


import controleurs.ControleurBoutonEncherir;
import controleurs.ControleurBoutonIconePhoto;
import controleurs.ControleurConsulterVente;
import exceptions.IntrouvableException;
import exceptions.ResultatVideException;
import interfaces.util.*;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import interfaces.util.Styles;
import modeles.*;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

public class ConsulterVente extends InterfaceModeConnecte
{

    private Vente vente;
    private Objet objet;
    private ControleurConsulterVente controleur;
    private List<Photo> photos;
    private List<Image> images;

    private BoutonIconeTexte contacterBtn;
    private BoutonIconeTexte retourBtn;
    private ImageCarreeCentree photo;
    private VBox content;

    private Label enchereActuelle;

    public ConsulterVente(Vente vente)
    {
        super();
        this.controleur = new ControleurConsulterVente(this);
        this.vente = vente;
        this.objet = vente.getObjet();
        this.photos = this.objet.getPhotos();
        this.images = new ArrayList<>();
        for (Photo p : this.photos)
        {
            byte[] b = p.getImg();
            Image image = new Image(new ByteArrayInputStream(b));
            this.images.add(image);
        }
        this.menuNavigation.selectionner(MenuNavigation.ICONE_VENTES);
        this.sceneConsulterVente();
        this.setCenter(content);
    }

    @Override
    protected VBox getContentPane()
    {
        return this.content;
    }

    private void sceneConsulterVente()
    {
        this.contacterBtn = new BoutonIconeTexte(ICONE_SEND, "Contacter le vendeur", 40, controleur);
        this.retourBtn = new BoutonIconeTexte(ICONE_RETOUR, "Retour", 40, controleur);

        try
        {
            int idUtVente = Requetes.recupererProprietaireVente(GestionnaireSQL.getConnexion(), this.vente.getId()).getId();
            this.contacterBtn.setDisable(idUtVente == ConnexionUtilisateur.getIdUtilisateurConnecte());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (IntrouvableException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : utilisateur introuvable", "Impossible de récupérer l'utilisateur associé à la vente.");
        }

        HBox barreAction = new BarreActions(objet.getNom(), this.contacterBtn, this.retourBtn);

        ScrollPane sc = partieCentrale();
        sc.setBackground(Styles.DEFAULT_BACKGROUND);

        content = new VBox();
        content.getChildren().addAll(barreAction, sc);
    }

    private ScrollPane partieCentrale()
    {

        VBox photoEtDesc = this.photoEtDesc();
        photoEtDesc.setBackground(Styles.DEFAULT_BACKGROUND);
        VBox iconsImage = this.iconsImage();
        VBox achat = this.vBoxAchat();
        Separator separateur = new Separator(Orientation.VERTICAL);

        HBox hBoxMid = new HBox(10);

        hBoxMid.getChildren().addAll(iconsImage, photoEtDesc, separateur, achat);
        hBoxMid.setMaxWidth(750);
        hBoxMid.setPadding(new Insets(20, 20, 20, 20));

        ScrollPane sc = new ScrollPane();
        sc.setContent(hBoxMid);
        sc.setPrefViewportHeight(600);
        sc.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        Styles.ajouterStyle(sc, Styles.SCROLLPANE_BACKGROUND);

        return sc;
    }

    private VBox iconsImage()
    {
        VBox listeImage = new VBox(10);
        for (Image image : this.images)
        {
            BoutonIcone photobtn = new BoutonIcone(image, 80, 80, new ControleurBoutonIconePhoto(this));
            photobtn.setStyle("-fx-border-color: #CCC;");
            listeImage.getChildren().add(photobtn);
        }
        return listeImage;
    }

    private VBox photoEtDesc()
    {
        Label description = new Label(objet.getDescription());
        description.setPrefWidth(400);
        description.setWrapText(true);
        description.setStyle(Styles.PARAGRAPHE);

        VBox photoEtDesc = new VBox(20);
        photo = new ImageCarreeCentree(images.get(0), 300);
        photoEtDesc.getChildren().addAll(photo, description);
        photoEtDesc.setPrefWidth(450);
        photoEtDesc.setAlignment(Pos.TOP_CENTER);
        return photoEtDesc;
    }

    private VBox vBoxAchat()
    {
        VBox achat = new VBox(15);
        achat.setAlignment(Pos.BASELINE_CENTER);

        Label lPrixDepart = new Label("Prix de départ : ");
        lPrixDepart.setStyle(Styles.TITRE_2);

        Label prixDepart = new Label("" + vente.getPrixBase());
        prixDepart.setStyle(Styles.PRIX_2);

        Label lEnchereActuelle = new Label("Enchère actuelle : ");
        lEnchereActuelle.setStyle(Styles.TITRE_2);


        Proposition propMax;

        try
        {
            propMax = Requetes.recupererPropositionMaximumPourVente(GestionnaireSQL.getConnexion(), this.getVente().getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            propMax = null;
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de trouver l'enchère la plus haute", "Une erreur est survenue lors de la récupération de l'enchère au montant le plus élevé dans la BD.");
        }
        catch (ResultatVideException e)
        {
            propMax = null;
        }


        this.enchereActuelle = new Label( propMax == null ? "Aucune enchère" : propMax.getMontant() + "€");
        enchereActuelle.setStyle(Styles.PRIX_1);

        Label lVendeur = new Label( propMax == null ?
                                      "soyez le premier à enchérir" :
                                      "par " + propMax.getUtilisateur().getPseudo()
                                             + (propMax.getUtilisateur().getId() == ConnexionUtilisateur.getIdUtilisateurConnecte() ? "(vous)" : ""));
        lVendeur.setStyle(Styles.ANNOTATION);

        BoutonHoverable encherir = new BoutonHoverable("Enchérir");
        encherir.setOnAction(new ControleurBoutonEncherir(this));
        if(propMax != null && propMax.getUtilisateur().getId() == ConnexionUtilisateur.getIdUtilisateurConnecte())
            encherir.setDisable(true);

        try
        {
            if ((Requetes.recupererUtilisateurParIdObjet(GestionnaireSQL.getConnexion(), vente.getObjet().getId()).getId() == ConnexionUtilisateur.getIdUtilisateurConnecte()))
                encherir.setDisable(true);
        }
        catch (SQLException e)
        {
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : Impossible de récupérer l'utilisateur", "Une erreur est survenue lors de la rcherche de l'utilisateur dans la base de données.");
        }
        catch (IntrouvableException e)
        {
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : Utilisateur introuvable", "L'utilisateur qui vend cet objet est introuvable.");
        }

        Label lVotreEnchere = new Label("Votre enchère : ");
        lVotreEnchere.setStyle(Styles.TITRE_2);

        Label votreEnchere = new Label("");
        try
        {
            votreEnchere.setText(Requetes.recupererPropositionUtilisateur(GestionnaireSQL.getConnexion(), this.vente.getId(), ConnexionUtilisateur.getIdUtilisateurConnecte()).getMontant() + "€");
        }

        catch(ResultatVideException e)
        {
            votreEnchere.setText("0€");
        }

        catch(SQLException e)
        {
            Util.alert(Alert.AlertType.ERROR, "", "", "");
        }
        votreEnchere.setStyle(Styles.PRIX_2);

        Label lDateFin = new Label("Date de clôture : ");
        Styles.ajouterStyle(lDateFin, Styles.TITRE_2);

        Label dateFin = new Label(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(this.vente.getDateFin()));
        Styles.ajouterStyle(dateFin, Styles.PRIX_2);

        achat.getChildren().addAll(lPrixDepart, prixDepart, lEnchereActuelle, enchereActuelle, lVendeur, encherir, lVotreEnchere, votreEnchere, lDateFin, dateFin);
        achat.setPrefWidth(300);
        achat.setAlignment(Pos.TOP_CENTER);
        return achat;
    }

    public void setPhoto(Image image)
    {
        this.photo.setImage(image);
    }

    public Vente getVente()
    {
        return this.vente;
    }

    public void rafraichir()
    {
        this.sceneConsulterVente();
    }
}
