package interfaces;

import interfaces.util.BarreMultipages;

public abstract class InterfaceMultipages extends InterfaceModeConnecte {
    private BarreMultipages bottom;
    protected int numPage;
    protected int nbObjetsParPage;
    protected int nbPages;
    protected int nbObjets;
    protected boolean changePage;

    public InterfaceMultipages(int nbObjets)
    {
        super();
        this.nbObjets = nbObjets;
        this.numPage = 1;
        this.nbObjetsParPage = 10;
        this.nbPages = ((int)Math.ceil(nbObjets / (double)nbObjetsParPage) == 0) ? 1 : (int)Math.ceil(nbObjets / (double)nbObjetsParPage);
        this.bottom = new BarreMultipages(this.numPage, this.nbPages, this);
        this.changePage = false;
        super.setBottom(this.bottom);
    }

    public void changePage(int change)
    {
        this.numPage += change;
        this.bottom.majAffichage(this.numPage, this.nbPages);
        this.setCenter(this.getContentPane());
    }

    public BarreMultipages getBarre()
    {
        return this.bottom;
    }

    public void setNbObjets(int nbObjets)
    {
        this.nbObjets = nbObjets;
        this.nbPages = ((int)Math.ceil(nbObjets / (double)nbObjetsParPage) == 0) ? 1 : (int)Math.ceil(nbObjets / (double)nbObjetsParPage);
        this.bottom.majAffichage(this.numPage, this.nbPages);
    }

    public int nbObjetsAffichables()
    {
        return (this.nbObjets - (this.numPage - 1) * this.nbObjetsParPage) > this.nbObjetsParPage ? this.nbObjetsParPage : (this.nbObjets - (this.numPage - 1) * this.nbObjetsParPage);
    }

    public void setChangePage(boolean value)
    {
        this.changePage = value;
    }
}
