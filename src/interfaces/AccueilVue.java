package interfaces;

import controleurs.ControleurMenuNavigation;
import exceptions.IntrouvableException;
import interfaces.util.BarreActions;
import interfaces.util.BoutonIconeTexte;
import interfaces.util.ImageCarreeCentree;
import interfaces.util.Styles;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import modeles.ConnexionUtilisateur;
import modeles.Utilisateur;
import modeles.Vente;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccueilVue extends InterfaceModeConnecte
{

    private ControleurMenuNavigation controleurMenuNavigation;

    public AccueilVue()
    {
        super();
        this.controleurMenuNavigation = new ControleurMenuNavigation();
        this.menuNavigation.selectionner(MenuNavigation.ICONE_HOME);
    }

    @Override
    protected BorderPane getContentPane() {
        BorderPane bp = new BorderPane();
        BarreActions barreActions = new BarreActions("Accueil");

        Utilisateur userConnnecte = null;

        try {
            userConnnecte = Requetes.recupererUtilisateurParId(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte());
        } catch (SQLException | IntrouvableException e) {
            e.printStackTrace();
        }

        VBox vBox = new VBox(20);
        vBox.setPadding(new Insets(0, 0, 0, 30));
        Label bienvenue = null;
        if (userConnnecte != null) {
            bienvenue = new Label("Bienvenue " + userConnnecte.getPseudo());
        }
        Styles.ajouterStyle(bienvenue, Styles.TITRE_2);
        bienvenue.setPadding(new Insets(30, 0, 0, 0));
        vBox.getChildren().add(bienvenue);

        BoutonIconeTexte ventesEnCours = null;
        try {
            int nbVente24h = Requetes.recupererVentesTerminantDansLes24H(GestionnaireSQL.getConnexion()).size();
            String texte;
            if (nbVente24h == 0) texte = "Aucune vente ne se terminent dans \nmoins de 24 heures";
            else if(nbVente24h == 1)texte = "1 vente se termine dans \nmoins de 24 heures";
            else texte = nbVente24h + " ventes se terminent dans \nmoins de 24 heures";
            ventesEnCours = new BoutonIconeTexte(MenuNavigation.ICONE_VENTES,  texte, 50, controleurMenuNavigation);
        } catch (SQLException e) {
            e.printStackTrace();
        }


        vBox.getChildren().add(ventesEnCours);

        BoutonIconeTexte ventesUserEncours = null;
        try {
            int nbVentesUtilisateurs = Requetes.recupererVentesUtilisateurEnCours(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte()).size();
            String texte;
            if (nbVentesUtilisateurs == 0) texte = "Vous n'avez aucune ventes en cours";
            else if(nbVentesUtilisateurs == 1)texte = "Vous avez 1 vente en cours";
            else texte = "Vous avez " + nbVentesUtilisateurs + " ventes en cours";
            ventesUserEncours = new BoutonIconeTexte(MenuNavigation.ICONE_MESVENTES, texte, 50, controleurMenuNavigation);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        vBox.getChildren().add(ventesUserEncours);

        BoutonIconeTexte propositions = null;
        try {
            int nbPropositions = Requetes.recupererEncheresUtilisateur(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte()).size();
            String texte;
            if (nbPropositions == 0) texte = "Vous n'avez aucune propositions \nen cours";
            else if(nbPropositions == 1)texte = "Vous avez 1 proposition en cours";
            else texte = "Vous avez " + nbPropositions + " ventes en cours";
            propositions = new BoutonIconeTexte(MenuNavigation.ICONE_MESPROPOSITIONS, texte, 50, controleurMenuNavigation);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        vBox.getChildren().add(propositions);

        BoutonIconeTexte messages = null;
        try {
            int nbNouveauxMessages = Requetes.recupererNbMessagesNonLus(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte());

            String texte;
            if (nbNouveauxMessages == 0) texte = "Vous n'avez aucun nouveaux \nmessages";
            else if(nbNouveauxMessages == 1)texte = "Vous avez 1 nouveau messages";
            else texte = "Vous avez " + nbNouveauxMessages + " nouveaux messages";
            messages = new BoutonIconeTexte(MenuNavigation.ICONE_MESSAGERIE, texte, 50, controleurMenuNavigation);
        } catch (SQLException | IntrouvableException e) {
            e.printStackTrace();
        }
        vBox.getChildren().add(messages);

        if (userConnnecte != null && userConnnecte.getRole().getId() == 1) {
            Label admin = new Label("Vous êtes administrateur");
            Styles.ajouterStyle(admin, Styles.GRAS);
            vBox.getChildren().add(admin);
        }

        bp.setTop(barreActions);
        bp.setCenter(vBox);
        FlowPane conteneurLogo = new FlowPane();
        ImageCarreeCentree logo = new ImageCarreeCentree(new Image("images/icones/home.png"), 250);
        conteneurLogo.getChildren().add(logo);
        conteneurLogo.setAlignment(Pos.BASELINE_CENTER);
        Label titre = new Label("Ventes Aux Enchères");
        Styles.ajouterStyle(titre, Styles.TITRE_2);
        Styles.ajouterStyle(titre, Styles.GRAS);
        VBox logoEtTitre = new VBox(10);
        logoEtTitre.getChildren().addAll(conteneurLogo, titre);
        logoEtTitre.setAlignment(Pos.TOP_CENTER);

        bp.setRight(logoEtTitre);

        return bp;
    }
}
