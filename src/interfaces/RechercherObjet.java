package interfaces;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import controleurs.ControleurAccesVente;
import controleurs.ControleurLigneQuadrillage;
import controleurs.ControleurRecherche;
import exceptions.ResultatVideException;
import interfaces.util.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import modeles.Vente;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RechercherObjet extends InterfaceMultipages implements IChangeMode, IRecherche {

    private BoutonIcone quadrillage;
    private BoutonIcone line;
    private TextField search;
    private List<CadreImageDescription> cadres;
    private List<Vente> ventes;
    private BoutonHoverable searchBtn;
    private boolean classic;
    private ScrollPane sp;

    public RechercherObjet()
    {
        super(0);
        this.ventes = new ArrayList<>();
        try {
            this.ventes.addAll(Requetes.recupererVentesEnCours(GestionnaireSQL.getConnexion()));
        } catch(SQLException e) {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de récupérer les ventes en cours", "Une erreur est survenue pendant la récupération des ventes en cours dans la base de données.");
        }
        this.classic = false;
        ControleurLigneQuadrillage controlAffichage = new ControleurLigneQuadrillage(this);
        ControleurRecherche controleurRecherche = new ControleurRecherche(this);
        this.menuNavigation.selectionner(MenuNavigation.ICONE_VENTES);
        this.quadrillage = new BoutonIcone(InterfaceModeConnecte.ICONE_QUADRILLAGE, 25, 25, controlAffichage);
        this.line = new BoutonIcone(InterfaceModeConnecte.ICONE_LINE, 25, 25, controlAffichage);
        this.search = new TextField();
        this.search.setOnAction(controleurRecherche);
        this.search.setPromptText("Rechercher un objet en enchère...");
        this.searchBtn = new BoutonHoverable("Rechercher");
        this.searchBtn.setOnAction(controleurRecherche);
        this.cadres = new ArrayList<>();
        this.sp = new ScrollPane();
        super.setNbObjets(this.ventes.size());
        super.stop();
    }

    @Override
    protected Node getContentPane() {
        try {
            this.ventes.addAll(Requetes.recupererVentesEnCours(GestionnaireSQL.getConnexion()));
        } catch(SQLException e) {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de récupérer les ventes en cours", "Une erreur est survenue pendant la récupération des ventes en cours dans la base de données.");
        }
        BorderPane main = new BorderPane();

        HBox barreAction1 = new HBox(5);
        barreAction1.setPadding(new Insets(5));
        barreAction1.getChildren().addAll(this.search, searchBtn);
        HBox.setHgrow(search, Priority.ALWAYS);

        HBox grid = new HBox(5);
        grid.getChildren().addAll(this.quadrillage,  this.line);

        HBox barreAction2 = new HBox(5);
        barreAction2.getChildren().add(grid);
        barreAction2.setPadding(new Insets(5));
        HBox.setHgrow(grid, Priority.ALWAYS);

        VBox barreAction = new VBox(5);
        barreAction.getChildren().addAll(barreAction1, barreAction2);

        if(super.changePage)
        {
            this.sp.setVvalue(0);
            super.changePage = false;
        }
        this.sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        this.sp.setContent(this.classic ? this.constructClassic() : this.constructPro());
        this.sp.setPadding(new Insets(20));
        Styles.ajouterStyle(sp, Styles.SCROLLPANE_BACKGROUND);

        main.setTop(barreAction);
        main.setCenter(this.sp);

        return main;
    }

    private GridPane constructClassic()
    {
        GridPane content = new GridPane();

        content.setAlignment(Pos.CENTER);
        content.setHgap(20);
        content.setVgap(content.getHgap());

        this.cadres.clear();
        for(int i = (this.numPage - 1) * this.nbObjetsParPage ; i < (this.numPage - 1) * this.nbObjetsParPage + super.nbObjetsAffichables() ; i++)
        {
            try
            {
                this.cadres.add(new CadreImageDescription(new Image(new ByteArrayInputStream(this.ventes.get(i).getObjet().getPhotos().get(0).getImg())), 350, 150, this.ventes.get(i).getObjet().getNom(), this.ventes.get(i).getObjet().getDescription(), 80, Requetes.recupererPropositionMaximumPourVente(GestionnaireSQL.getConnexion(), this.ventes.get(i).getId()).getMontant() + "€", new ControleurAccesVente(this.ventes.get(i))));
            }

            catch(SQLException e)
            {
                Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de récupérer la vente", "Une erreur est survenue pendant l'accès à la base de données.");
            }

            catch(ResultatVideException e)
            {
                this.cadres.add(new CadreImageDescription(new Image(new ByteArrayInputStream(this.ventes.get(i).getObjet().getPhotos().get(0).getImg())), 350, 150, this.ventes.get(i).getObjet().getNom(), this.ventes.get(i).getObjet().getDescription(), 80, this.ventes.get(i).getPrixBase() + "€", new ControleurAccesVente(this.ventes.get(i))));
            }

            catch(IndexOutOfBoundsException e)
            {
                System.out.println("loser");
            }
        }

        int y = 0;
        for(int i = 0; i < this.cadres.size(); i++)
        {
            if(i % 2 == 0)
                y++;

            content.add(this.cadres.get(i), i % 2, y);
        }

        return content;
    }

    private TableView<EntreeTableView> constructPro()
    {
        TableView<EntreeTableView> tv = new TableView<>();
        tv.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tv.setPrefHeight(378);
        tv.setPrefWidth(743);
        tv.setBackground(Background.EMPTY);

        tv.getColumns().addAll(new TableColumn<EntreeTableView, String>("Nom"), new TableColumn<EntreeTableView, String>("Description"), new TableColumn<EntreeTableView, String>("Prix"), new TableColumn<EntreeTableView, Hyperlink>(""));
        tv.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        tv.getColumns().get(0).prefWidthProperty().bind(tv.widthProperty().multiply(1.0/tv.getColumns().size()));
        tv.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("description"));
        tv.getColumns().get(1).prefWidthProperty().bind(tv.widthProperty().multiply(1.0/tv.getColumns().size()));
        tv.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("prix"));
        tv.getColumns().get(2).prefWidthProperty().bind(tv.widthProperty().multiply(1.0/tv.getColumns().size()));
        tv.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("details"));
        tv.getColumns().get(3).prefWidthProperty().bind(tv.widthProperty().multiply(1.0/tv.getColumns().size()));
        tv.getColumns().get(3).setStyle(tv.getColumns().get(2).getStyle() +  "-fx-alignment: CENTER-RIGHT;");

        tv.setItems(getObjectsList());

        // PERMET D'EMPECHER DE REORDONNER LES COLONNES DE LA TABLEVIEW
        tv.widthProperty().addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth)
            {
                TableHeaderRow header = (TableHeaderRow) tv.lookup("TableHeaderRow");
                header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                        header.setReordering(false);
                    }
                });
            }
        });

        return tv;
    }

    private ObservableList<EntreeTableView> getObjectsList()
    {
        List<EntreeTableView> list = new ArrayList<>();
        for(Vente v : this.ventes)
            list.add(new EntreeTableViewVente(v));

        return FXCollections.observableArrayList(list);
    }

    @Override
    public void changeAffichage(boolean valeur)
    {
        this.classic = valeur;
        this.getInterface();
    }

    @Override
    public void recherche(String recherche)
    {
        try
        {
            this.ventes = Requetes.rechercheVente(GestionnaireSQL.getConnexion(), recherche);
            super.setNbObjets(this.ventes.size());
        }

        catch(SQLException e)
        {
            Util.alert(Alert.AlertType.ERROR, "", "", "");
        }

        this.getInterface();
    }

    @Override
    public TextField getSearch()
    {
        return search;
    }
}
