package interfaces;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import controleurs.ControleurAccesVente;
import controleurs.ControleurLigneQuadrillage;
import controleurs.ControleurRecherche;
import exceptions.ResultatVideException;
import interfaces.util.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import modeles.ConnexionUtilisateur;
import modeles.Proposition;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MesPropositions extends InterfaceMultipages implements IChangeMode, IRecherche {
    private BoutonIcone quadrillage;
    private BoutonIcone line;
    private TextField search;
    private List<Proposition> propositions;
    private List<CadreImageDescriptionAlternatif> cadres;
    private boolean classic;
    private ScrollPane sp;

    public MesPropositions()
    {
        super(0);
        this.propositions = new ArrayList<>();
        try
        {
            this.propositions.addAll(Requetes.recupererEncheresUtilisateur(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte()));
        }

        catch(SQLException e)
        {
            Util.alert(Alert.AlertType.ERROR, "", "", "");
        }
        ControleurLigneQuadrillage controlAffichage = new ControleurLigneQuadrillage(this);
        this.sp = new ScrollPane();
        this.menuNavigation.selectionner(MenuNavigation.ICONE_MESPROPOSITIONS);
        this.classic = true;
        this.cadres = new ArrayList<>();
        this.quadrillage = new BoutonIcone(InterfaceModeConnecte.ICONE_QUADRILLAGE, 25, 25, controlAffichage);
        this.line = new BoutonIcone(InterfaceModeConnecte.ICONE_LINE, 25, 25, controlAffichage);
        this.search = new TextField();
        this.search.setOnAction(new ControleurRecherche(this));
        this.search.setPromptText("Rechercher...");

        super.setNbObjets(this.propositions.size());
    }

    @Override
    protected Node getContentPane() {
        try
        {
            this.propositions.clear();
            this.propositions.addAll(Requetes.recupererEncheresUtilisateur(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte()));
            super.setNbObjets(this.propositions.size());
        }

        catch(SQLException e)
        {
            Util.alert(Alert.AlertType.ERROR, "", "", "");
        }

        BorderPane main = new BorderPane();
        BarreActions ba = new BarreActions("Mes propositions", this.quadrillage, this.line, this.search);

        if(super.changePage)
        {
            this.sp.setVvalue(0);
            super.changePage = false;
        }
        this.sp.setPrefHeight(500);
        this.sp.setPadding(new Insets(20, 0, 0, 20));
        this.sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        Styles.ajouterStyle(this.sp, Styles.SCROLLPANE_BACKGROUND);
        this.sp.setContent(this.classic ? this.constructClassic() : this.constructPro());

        main.setTop(ba);
        main.setCenter(this.sp);

        return main;
    }

    private GridPane constructClassic()
    {
        GridPane content = new GridPane();

        content.setAlignment(Pos.CENTER);
        content.setHgap(20);
        content.setVgap(content.getHgap());

        this.cadres.clear();
        for(int i = (this.numPage - 1) * this.nbObjetsParPage ; i < (this.numPage - 1) * this.nbObjetsParPage + super.nbObjetsAffichables() ; i++)
        {
            try
            {
                this.cadres.add(new CadreImageDescriptionAlternatif(new Image(new ByteArrayInputStream(this.propositions.get(i).getVente().getObjet().getPhotos().get(0).getImg())), 350, 150, this.propositions.get(i).getVente().getObjet().getNom(), this.propositions.get(i).getVente().getObjet().getDescription(), 40, this.propositions.get(i).getMontant() + "€", Requetes.recupererPropositionMaximumPourVente(GestionnaireSQL.getConnexion(), this.propositions.get(i).getVente().getId()).getMontant() + "€", false, new ControleurAccesVente(this.propositions.get(i).getVente())));
            }

            catch(SQLException e)
            {
                Util.alert(Alert.AlertType.ERROR, "", "", "");
            }

            catch(ResultatVideException e)
            {
                this.cadres.add(new CadreImageDescriptionAlternatif(new Image(new ByteArrayInputStream(this.propositions.get(i).getVente().getObjet().getPhotos().get(0).getImg())), 350, 150, this.propositions.get(i).getVente().getObjet().getNom(), this.propositions.get(i).getVente().getObjet().getDescription(), 40, this.propositions.get(i).getMontant() + "€", "0€", false, new ControleurAccesVente(this.propositions.get(i).getVente())));
            }
        }
        int y = 0;
        for(int i = 0; i < this.cadres.size(); i++)
        {
            if(i % 2 == 0)
                y++;

            content.add(this.cadres.get(i), i % 2, y);
        }

        return content;
    }

    private TableView<EntreeTableView> constructPro()
    {
        TableView<EntreeTableView> tv = new TableView<>();
        tv.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tv.setPrefHeight(407);
        tv.setPrefWidth(760);
        tv.setBackground(Background.EMPTY);

        tv.getColumns().addAll(new TableColumn<EntreeTableView, String>("Nom"), new TableColumn<EntreeTableView, String>("Description"), new TableColumn<EntreeTableView, String>("Enchère"), new TableColumn<EntreeTableView, Hyperlink>(""));
        tv.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        tv.getColumns().get(0).prefWidthProperty().bind(tv.widthProperty().multiply(1.0/tv.getColumns().size()));
        tv.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("description"));
        tv.getColumns().get(1).prefWidthProperty().bind(tv.widthProperty().multiply(1.0/tv.getColumns().size()));
        tv.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("enchere"));
        tv.getColumns().get(2).prefWidthProperty().bind(tv.widthProperty().multiply(1.0/tv.getColumns().size()));
        tv.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("details"));
        tv.getColumns().get(3).prefWidthProperty().bind(tv.widthProperty().multiply(1.0/tv.getColumns().size()));
        tv.getColumns().get(3).setStyle(tv.getColumns().get(2).getStyle() +  "-fx-alignment: CENTER-RIGHT;");

        tv.setItems(getObjectsList());

        // PERMET D'EMPECHER DE REORDONNER LES COLONNES DE LA TABLEVIEW
        tv.widthProperty().addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth)
            {
                TableHeaderRow header = (TableHeaderRow) tv.lookup("TableHeaderRow");
                header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                        header.setReordering(false);
                    }
                });
            }
        });

        return tv;
    }

    private ObservableList<EntreeTableView> getObjectsList()
    {
        List<EntreeTableView> list = new ArrayList<>();
        for(Proposition p : this.propositions)
            list.add(new EntreeTableViewProposition(p));

        return FXCollections.observableArrayList(list);
    }

    @Override
    public void changeAffichage(boolean valeur)
    {
        this.classic = valeur;
        this.getInterface();
    }

    @Override
    public TextField getSearch()
    {
        return this.search;
    }

    @Override
    public void recherche(String recherche)
    {
        try
        {
            this.propositions = Requetes.rechercheProposition(GestionnaireSQL.getConnexion(), recherche, ConnexionUtilisateur.getIdUtilisateurConnecte());
            super.setNbObjets(this.propositions.size());
        }

        catch(SQLException e)
        {
            Util.alert(Alert.AlertType.ERROR, "", "", "");
        }

        this.getInterface();
    }
}
