package interfaces;

import controleurs.ControleurMessagerie;
import exceptions.IntrouvableException;
import interfaces.util.*;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import modeles.*;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Messagerie extends InterfaceMultipages
{
    public static final Image ICONE_NOUVEAU_MSG = new Image("images/icones/send.png"),
                              ICONE_SUPPRIMER_MSG = new Image("images/icones/trash.png");

    private List<Conversation> conversations;

    // TODO implementer affichage msg non lu
    // TODO implementer auto refresh
    public Messagerie()
    {
        super(0);
        this.menuNavigation.selectionner(MenuNavigation.ICONE_MESSAGERIE);
        try
        {
            this.conversations = Requetes.recupererConversationsUtilisateur(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de récupérer les messages", "Une erreur a empếché la récupération de vos messages. Essayez à nouveau d'ouvrir la page. Ne vous inquiétez pas, aucune donnée n'a été perdue.");
        }
        catch (IntrouvableException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR,"Erreur", "Erreur : impossible de récupérer les informations d'utilisateur", "Une erreur est survenue lors de la récupération de vos informations dans la base de données. Aucune donnée n'a été perdue.");
        }

        this.setNbObjets(this.conversations.size());
    }

    @Override
    protected Node getContentPane()
    {
        try
        {
            this.conversations = Requetes.recupererConversationsUtilisateur(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte());
            this.setNbObjets(this.conversations.size());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de récupérer les messages", "Une erreur a empếché la récupération de vos messages. Essayez à nouveau d'ouvrir la page. Ne vous inquiétez pas, aucune donnée n'a été perdue.");
        }
        catch (IntrouvableException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR,"Erreur", "Erreur : impossible de récupérer les informations d'utilisateur", "Une erreur est survenue lors de la récupération de vos informations dans la base de données. Aucune donnée n'a été perdue.");
        }
        BorderPane contenu = new BorderPane();

        ControleurMessagerie controleurMessagerie = new ControleurMessagerie(this);

        Label espacementLbl = new Label("");
        espacementLbl.setPadding(new Insets(15, 0, 10, 0));
        BarreActions actions = new BarreActions("Messagerie", espacementLbl);
        contenu.setTop(actions);

        List<Conversation> affichage = this.conversations.subList((this.numPage - 1) * nbObjetsParPage, (this.numPage - 1) * nbObjetsParPage + this.nbObjetsAffichables());

        VBox conteneurAffichage = new VBox();
        conteneurAffichage.setPadding(new Insets(10, 15, 10, 15));
        conteneurAffichage.setSpacing(10);
        Styles.ajouterStyle(conteneurAffichage, "-fx-background: white; -fx-background-color: white;");

        for(Conversation conv : affichage)
        {
            conteneurAffichage.getChildren().add(new EntreeListeConversations(conv));
        }

        ScrollPane sp = new ScrollPane();
        sp.setFitToWidth(true);
        sp.setFitToHeight(true);
        Styles.ajouterStyle(sp, Styles.SCROLLPANE_BACKGROUND);
        sp.setContent(conteneurAffichage);

        contenu.setCenter(sp);

        return contenu;
    }
}
