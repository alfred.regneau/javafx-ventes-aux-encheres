package interfaces;

import controleurs.ControleurConnexion;
import controleurs.GestionnairesFenetres;
import interfaces.util.Styles;
import interfaces.util.Util;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class ConnexionVue extends VBox
{

    private TextField userId; // TextField pour rentrer son Identifiant
    private Label userIdSublabel;
    private TextField mdp; // TextField pour rentrer son mot de passe
    private Label mdpSublabel;
    private Button connexionBtn;

    public ConnexionVue()
    {
        super(20);

        ControleurConnexion controleur = new ControleurConnexion(this);

        Label titrePage = new Label("Connexion");
        Styles.ajouterStyle(titrePage, Styles.TITRE_1);
        VBox.setMargin(titrePage, new Insets(90, 0, 20, 0));

        VBox zoneConnexion = creerZoneIdentification(controleur);


        Separator sep = new Separator();
        VBox.setMargin(sep, new Insets(0, 25, 0, 25));

        HBox registerBox = creerZoneInscription(); // créer  la partie ou l'utilisateur peut s'inscrire
        VBox.setMargin(registerBox, new Insets(0, 0, 25, 0));

        this.connexionBtn = new Button("Se connecter");
        this.connexionBtn.setOnAction(controleur);
        this.connexionBtn.setPadding(new Insets(10, 20, 10, 20));
        VBox.setMargin(this.connexionBtn, new Insets(0, 0, 15, 0));

        this.getChildren().addAll(titrePage, zoneConnexion, this.connexionBtn, sep, registerBox);


        this.setAlignment(Pos.CENTER); // modification de l'alignement des élements
    }

    private HBox creerZoneInscription()
    {
        HBox inscrire = new HBox();
        Label description = new Label("Vous n'avez pas de compte ? Créez-en un");
        Styles.ajouterStyle(description, Styles.ANNOTATION);

        Hyperlink register = new Hyperlink("ici");
        register.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                GestionnairesFenetres.afficherInterfaceInscription();
            }
        });

        inscrire.getChildren().add(description);
        inscrire.getChildren().add(register);

        inscrire.setAlignment(Pos.CENTER);
        return inscrire;
    }

    public VBox creerZoneIdentification(EventHandler<ActionEvent> eventHandler)
    {
        VBox conteneurIdentifiant = new VBox();

        this.userId = new TextField();
        this.userId.setOnAction(eventHandler);
        this.userId.setPromptText("Identifiant/Adresse mail"); // placeholder
        VBox.setMargin(this.userId, new Insets(0, 40, 0, 40));

        this.userIdSublabel = new Label("");
        Styles.ajouterStyle(this.userIdSublabel, Styles.ERREUR_ANNOTATION);

        this.mdp = new PasswordField();
        this.mdp.setOnAction(eventHandler);
        this.mdp.setPromptText("Mot de passe"); // placeholder
        VBox.setMargin(this.mdp, new Insets(0, 40, 0, 40));

        this.mdpSublabel = new Label("");
        Styles.ajouterStyle(this.mdpSublabel, Styles.ERREUR_ANNOTATION);

        Separator sep = new Separator();
        Hyperlink mdpOublie = new Hyperlink("Identifiant ou mot de passe oublié ?"); // création d'un hyperlien
        mdpOublie.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                // TODO systeme de reset mot de passe
                Util.alert(Alert.AlertType.WARNING, "À terminer", "Fonctionnalité non implémentée", "Cette fonctionnalité n'a pas encore été implémentée.");
            }
        });


        conteneurIdentifiant.getChildren().addAll(this.userId, this.userIdSublabel, this.mdp, this.mdpSublabel, sep, mdpOublie);

        conteneurIdentifiant.setBorder(Styles.LIGHTGRAY_BORDER);
        conteneurIdentifiant.setPadding(new Insets(40, 15, 35, 20));
        conteneurIdentifiant.setAlignment(Pos.CENTER);
        conteneurIdentifiant.setSpacing(10);
        conteneurIdentifiant.setMaxWidth(450);

        return conteneurIdentifiant;
    }

    public String getNomUtilisateur()
    {
        return this.userId.getText();
    }

    public String getMotDePasse()
    {
        return this.mdp.getText();
    }

    public void identifiantInconnu()
    {
        this.userIdSublabel.setText("Identifiant inconnu");
    }

    public void motDePasseIncorrect()
    {
        this.mdpSublabel.setText("Mot de passe incorrect");
    }

    public void reinitSublabels()
    {
        this.userIdSublabel.setText("");
        this.mdpSublabel.setText("");
    }
}
