package interfaces;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import controleurs.ControleurAccesVente;
import controleurs.ControleurAcceAjouterVente;
import controleurs.ControleurLigneQuadrillage;
import controleurs.ControleurMesVentes;
import exceptions.ResultatVideException;
import interfaces.util.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import modeles.*;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;


import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MesVentes extends InterfaceModeConnecte implements IChangeMode {

    private BoutonIcone quadrillage;
    private BoutonIcone line;
    private Label enCours;
    private Label terminees;
    private List<Vente> ventes;
    private List<Vente> venteEnCours;
    private List<Vente> venteTerminees;
    private Pane mesVentesEnCours;
    private Pane mesVentesTerminees;
    private boolean classic;
    private ControleurLigneQuadrillage controlAffichage;
    private VBox content;
    private ScrollPane sp;

    public MesVentes(){
        super();
        this.classic = false;
        this.controlAffichage = new ControleurLigneQuadrillage(this);
        this.quadrillage = new BoutonIcone(ICONE_QUADRILLAGE, 25, 25, this.controlAffichage);
        this.line = new BoutonIcone(ICONE_LINE, 25, 25, this.controlAffichage);
        this.menuNavigation.selectionner(MenuNavigation.ICONE_MESVENTES);

        try {
            this.ventes = Requetes.recupererVentesUtilisateur(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte());
        } catch (SQLException e) {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "", "", "");
        }
        this.venteEnCours = new ArrayList<>();
        this.venteTerminees = new ArrayList<>();
        /*for(Vente v : ventes){
            switch(v.getStatut().getIdStatut()){
                //0 = à venir 1 = en cours 2 = à valider
                case 0: case 1: case 2:
                    venteEnCours.add(v);
                    break;
                //3 = à valider 4 = non conclue
                case 3: case 4:
                    venteTerminees.add(v);
                    break;
                default:
                    break;
            }
        }*/
        /*pour ajouter 5 ventes à venir, 5 en cours, et 5 terminées*/
//        for(int i = 0; i < 5 ;i++) {
//            this.ventes.add(new Vente(1, 1.0, 1.0, new Date(2019, 6, 13), new Date(2019, 6, 15), new Objet(1, "Table", "Table en marbre du 13ème siècle", new Categorie(1, "Meuble"))));
//        }
//        for(int i = 0; i < 5 ;i++) {
//            Vente vente = new Vente(1, 10.0, 1.0, new Date(2019, 6, 13), new Date(2019, 6, 15), new Objet(1, "Vélo", "Vélo Tout Terrain pour enfant", new Categorie(1, "Sport")));
//            vente.setStatutVente(new Statut(1, "En cours"));
//            this.ventes.add(vente);
//        }
//        for(int i = 0; i < 5 ;i++) {
//            Vente vente = new Vente(1, 10.0, 1.0, new Date(2019, 6, 13), new Date(2019, 6, 15), new Objet(1, "Vélo", "Vélo Tout Terrain pour enfant", new Categorie(1, "Sport")));
//            vente.setStatutVente(new Statut(2, "Terminees"));
//            this.ventes.add(vente);
//        }
        for (Vente v : ventes){
            if (v.getStatut().getIdStatut() == 0 || v.getStatut().getIdStatut() == 1)
                this.venteEnCours.add(v);
            else
                this.venteTerminees.add(v);
        }
        this.sp = new ScrollPane();
    }

    @Override
    protected BorderPane getContentPane() {
        try {
            this.ventes = Requetes.recupererVentesUtilisateur(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte());
            this.venteTerminees.clear();
            this.venteEnCours.clear();
            for (Vente v : ventes){
                if (v.getStatut().getIdStatut() == 0 || v.getStatut().getIdStatut() == 1)
                    this.venteEnCours.add(v);
                else
                    this.venteTerminees.add(v);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "", "", "");
        }
        content = new VBox();
        BoutonIconeTexte ajouterVente = new BoutonIconeTexte(new Image("images/icones/plus.png"),"Ajouter Vente",40,new ControleurAcceAjouterVente(this));
        BarreActions barreActions = new BarreActions("Mes ventes", ajouterVente, this.quadrillage, this.line);

        enCours = new Label("En cours");
        enCours.setPadding(new Insets(30, 0, 0, 0));
        Styles.ajouterStyle(enCours, Styles.TITRE_2);
        terminees = new Label("Terminées");
        terminees.setPadding(new Insets(30, 0, 0, 0));
        Styles.ajouterStyle(terminees, Styles.TITRE_2);
        majAffichage();

        content.setPadding(new Insets(0, 0, 0, 30));
        this.sp.setContent(content);
        this.sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        Styles.ajouterStyle(sp, Styles.SCROLLPANE_BACKGROUND);
        BorderPane bp = new BorderPane();
        bp.setTop(barreActions);
        bp.setCenter(sp);
        return bp;
    }


    private GridPane mesVentesEnCoursQuadrillage(){
        GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setHgap(10);
        gp.setVgap(gp.getHgap());

        List<CadreImageDescription> cadreEncours = new ArrayList<>();
        for(Vente v : venteEnCours)
        {
            try
            {
                cadreEncours.add(new CadreImageDescription(new Image(new ByteArrayInputStream(v.getObjet().getPhotos().get(0).getImg())), 350, 150, v.getObjet().getNom(), v.getObjet().getDescription(), 80, Requetes.recupererPropositionMaximumPourVente(GestionnaireSQL.getConnexion(), v.getId()).getMontant() + "€", new ControleurAccesVente(v)));
            }

            catch(SQLException e)
            {
                Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de récupérer la vente", "Une erreur est survenue pendant l'accès à la base de données.");
            }

            catch(ResultatVideException e)
            {
                cadreEncours.add(new CadreImageDescription(new Image(new ByteArrayInputStream(v.getObjet().getPhotos().get(0).getImg())), 350, 150, v.getObjet().getNom(), v.getObjet().getDescription(), 80, v.getPrixBase() + "€", new ControleurAccesVente(v)));
            }

            catch(IndexOutOfBoundsException e)
            {
                System.out.println("loser");
            }
        }

        int y = 0;
        for(int i = 0; i < cadreEncours.size(); i++)
        {
            if(i % 2 == 0)
                y++;

            gp.add(cadreEncours.get(i), i % 2, y);
        }
        return gp;
    }


    private VBox mesVentesEnCoursEnLigne()
    {
        VBox content = new VBox(20);
        content.setMaxWidth(700);
        content.setPadding(new Insets(10, 0, 0, 10));
        TableView<EntreeTableViewMesVentes> tv = new TableView<>();
        tv.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        Styles.ajouterStyle(tv, Styles.SCROLLPANE_BACKGROUND);
        tv.setMaxHeight(240);

        tv.getColumns().addAll(new TableColumn<>("Objet"), new TableColumn<>("Statut"), new TableColumn<>("Enchère"), new TableColumn<>("Actions"));
        tv.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("nomEtDetails"));
        tv.getColumns().get(0).setMinWidth(250);
        tv.getColumns().get(0).setMaxWidth(250);
        tv.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("statut"));
        tv.getColumns().get(1).setMinWidth(150);
        tv.getColumns().get(1).setMaxWidth(150);
        tv.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("prix"));
        tv.getColumns().get(2).setMinWidth(140);
        tv.getColumns().get(2).setMaxWidth(140);
        tv.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("action"));
        tv.getColumns().get(3).setMinWidth(128);
        tv.getColumns().get(3).setMaxWidth(128);
        tv.getColumns().get(3).setStyle(tv.getColumns().get(2).getStyle() +  "-fx-alignment: CENTER;");

        tv.setItems(getObjectsList(1, 2, 3));

        // PERMET D'EMPECHER DE REORDONNER LES COLONNES DE LA TABLEVIEW
        tv.widthProperty().addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth)
            {
                TableHeaderRow header = (TableHeaderRow) tv.lookup("TableHeaderRow");
                header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                        header.setReordering(false);
                    }
                });
            }
        });

        content.getChildren().add(tv);

        return content;
    }

    private GridPane mesVentesTermineesQuadrillage(){
        GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setHgap(10);
        gp.setVgap(gp.getHgap());

        List<CadreImageDescription> cadreTerminees = new ArrayList<>();
        for(Vente v : venteTerminees)
        {
            try
            {
                cadreTerminees.add(new CadreImageDescription(new Image(new ByteArrayInputStream(v.getObjet().getPhotos().get(0).getImg())), 350, 150, v.getObjet().getNom(), v.getObjet().getDescription(), 80, Requetes.recupererPropositionMaximumPourVente(GestionnaireSQL.getConnexion(), v.getId()).getMontant() + "€", new ControleurAccesVente(v)));
            }

            catch(SQLException e)
            {
                Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de récupérer la vente", "Une erreur est survenue pendant l'accès à la base de données.");
            }

            catch(ResultatVideException e)
            {
                cadreTerminees.add(new CadreImageDescription(new Image(new ByteArrayInputStream(v.getObjet().getPhotos().get(0).getImg())), 350, 150, v.getObjet().getNom(), v.getObjet().getDescription(), 80, v.getPrixBase() + "€", new ControleurAccesVente(v)));
            }

            catch(IndexOutOfBoundsException e)
            {
                System.out.println("loser");
            }
        }
        int y = 0;
        for(int i = 0; i < cadreTerminees.size(); i++)
        {
            if(i % 2 == 0)
                y++;

            gp.add(cadreTerminees.get(i), i % 2, y);
        }
        return gp;
    }

    private VBox mesVentesTermineesEnLigne()
    {
        VBox content = new VBox(20);
        content.setPrefWidth(700);
        content.setPadding(new Insets(10, 0, 20, 10));
        TableView<EntreeTableViewMesVentes> tv = new TableView<>();
        tv.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        Styles.ajouterStyle(tv, Styles.SCROLLPANE_BACKGROUND);
        tv.setMaxHeight(400);

        tv.getColumns().addAll(new TableColumn<>("Objet"), new TableColumn<>("Statut"), new TableColumn<>("Enchère finale"));
        tv.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("nomEtDetails"));
        tv.getColumns().get(0).setMinWidth(300);
        tv.getColumns().get(0).setMaxWidth(300);
        tv.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("statut"));
        tv.getColumns().get(1).setMinWidth(185);
        tv.getColumns().get(1).setMaxWidth(185);
        tv.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("prix"));
        tv.getColumns().get(2).setMinWidth(185);
        tv.getColumns().get(2).setMaxWidth(185);
//        tv.getColumns().addAll(new TableColumn<>("Objet"), new TableColumn<>("Statut"), new TableColumn<>("Enchère finale"), new TableColumn<>("Action"));
//        tv.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("nomEtDetails"));
//        tv.getColumns().get(0).setMinWidth(250);
//        tv.getColumns().get(0).setMaxWidth(250);
//        tv.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("statut"));
//        tv.getColumns().get(1).setMinWidth(150);
//        tv.getColumns().get(1).setMaxWidth(150);
//        tv.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("prix"));
//        tv.getColumns().get(2).setMinWidth(140);
//        tv.getColumns().get(2).setMaxWidth(140);
//        tv.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("action"));
//        tv.getColumns().get(3).setMinWidth(128);
//        tv.getColumns().get(3).setMaxWidth(128);
        tv.setItems(getObjectsList(4, 5));

        // PERMET D'EMPECHER DE REORDONNER LES COLONNES DE LA TABLEVIEW
        tv.widthProperty().addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth)
            {
                TableHeaderRow header = (TableHeaderRow) tv.lookup("TableHeaderRow");
                header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                        header.setReordering(false);
                    }
                });
            }
        });

        content.getChildren().add(tv);

        return content;


    }

    private ObservableList<EntreeTableViewMesVentes> getObjectsList(int... statut) {
        List<EntreeTableViewMesVentes> list = new ArrayList<>();
        for (Vente v : this.ventes)
            for(int s : statut)
            {
                if (v.getStatut().getIdStatut() == s)
                list.add(new EntreeTableViewMesVentes(this, v));
            }

        return FXCollections.observableArrayList(list);
    }

    public void majAffichage() {
        if (!this.classic) {

            mesVentesEnCours = mesVentesEnCoursEnLigne();
            mesVentesTerminees = mesVentesTermineesEnLigne();

        } else {

            mesVentesEnCours = mesVentesEnCoursQuadrillage();
            mesVentesTerminees = mesVentesTermineesQuadrillage();

        }

        content.getChildren().clear();
        content.getChildren().addAll(enCours, mesVentesEnCours, terminees, mesVentesTerminees);
    }

    @Override
    public void changeAffichage(boolean valeur)
    {
        this.classic = valeur;
        this.majAffichage();
    }
}
