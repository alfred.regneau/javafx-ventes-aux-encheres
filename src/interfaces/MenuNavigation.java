package interfaces;

import controleurs.ControleurMenuNavigation;
import interfaces.util.BoutonIcone;
import interfaces.util.BoutonIconeTexte;
import interfaces.util.Styles;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MenuNavigation extends AnchorPane
{

    private BoutonIcone accueil,
            ventes,
            mesVentes,
            mesPropositions,
            mesAcquisitions,
            messagerie,
            deconnexion;

    private List<BoutonIcone> boutons;

    private EventHandler<MouseEvent> hoverHandler, unhoverHandler;

    public static final Image ICONE_HOME = new Image("images/icones/home.png"),
                               ICONE_VENTES = new Image("images/icones/ventes.png"),
                               ICONE_MESVENTES = new Image("images/icones/mes_ventes.png"),
                               ICONE_MESPROPOSITIONS = new Image("images/icones/mes_propositions.png"),
                               ICONE_MESACQUISITIONS = new Image("images/icones/mes_acquisitions.png"),
                               ICONE_MESSAGERIE = new Image("images/icones/messagerie.png"),
                               ICONE_DECONNEXION = new Image("images/icones/deconnexion.png");

    public MenuNavigation()
    {
        this.initialiserBoutons();

        Separator sep = new Separator();
        sep.setOrientation(Orientation.VERTICAL);

        HBox partieGauche = new HBox();
        partieGauche.getChildren().addAll(this.accueil, sep, this.ventes, this.mesVentes, this.mesPropositions, this.mesAcquisitions);
        partieGauche.setAlignment(Pos.CENTER_LEFT);
        partieGauche.setSpacing(0);

        AnchorPane.setLeftAnchor(partieGauche, 0.0);


        HBox partieDroite = new HBox();
        partieDroite.getChildren().addAll(this.messagerie, this.deconnexion);
        partieDroite.setAlignment(Pos.CENTER_LEFT);
        partieDroite.setSpacing(0);

        AnchorPane.setRightAnchor(partieDroite, 0.0);


        this.getChildren().addAll(partieGauche, partieDroite);

        this.setBackground(Styles.NAVBAR_BACKGROUND);
        this.setBorder(Styles.BLACK_BOTTOM_BORDER);
    }

    private void initialiserBoutons()
    {
        ControleurMenuNavigation controleurMenuNavigation = new ControleurMenuNavigation();

        this.accueil = new BoutonIcone(ICONE_HOME, 40, 40, controleurMenuNavigation);
        this.ventes = new BoutonIconeTexte(ICONE_VENTES, "Ventes en cours", 40, controleurMenuNavigation);
        this.mesVentes = new BoutonIconeTexte(ICONE_MESVENTES, "Mes ventes", 40, controleurMenuNavigation);
        this.mesPropositions = new BoutonIconeTexte(ICONE_MESPROPOSITIONS, "Mes propositions", 40, controleurMenuNavigation);
        this.mesAcquisitions = new BoutonIconeTexte(ICONE_MESACQUISITIONS, "Mes acquisitions", 40, controleurMenuNavigation);
        this.messagerie = new BoutonIcone(ICONE_MESSAGERIE, 40, 40, controleurMenuNavigation);
        this.deconnexion = new BoutonIcone(ICONE_DECONNEXION, 40, 40, controleurMenuNavigation);

        // on remplace la mise en forme du texte par défaut des boutons par celle de la navbar
        Styles.ecraserStyle(this.ventes, Styles.TEXTE_BOUTON_NAVBAR);
        Styles.ecraserStyle(this.mesVentes, Styles.TEXTE_BOUTON_NAVBAR);
        Styles.ecraserStyle(this.mesPropositions, Styles.TEXTE_BOUTON_NAVBAR);
        Styles.ecraserStyle(this.mesAcquisitions, Styles.TEXTE_BOUTON_NAVBAR);

        boutons = new ArrayList<>();
        boutons.addAll(Arrays.asList(this.accueil, this.ventes, this.mesVentes, this.mesPropositions, this.mesAcquisitions, this.messagerie, this.deconnexion));
    }

    public void selectionner(Image iconeBtn)
    {
        for(BoutonIcone btn : this.boutons)
        {
            if(btn.getIcone() == iconeBtn)
                btn.setSelectionne(true);
            else
                btn.setSelectionne(false);
        }
    }
}
