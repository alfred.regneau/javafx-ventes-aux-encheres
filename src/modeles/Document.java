package modeles;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class Document {
    private int id;
    private String titre;
    private String contenu;
    private Date date;

    public Document(int id, String titre, String contenu, Date date) {
        this.id = id;
        this.titre = titre;
        this.contenu = contenu;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getContenu() {
        return contenu;
    }

    public Date getDate() {
        return date;
    }

    public static Document lireDocument(ResultSet res) throws SQLException{
        Document newDoc = new Document (res.getInt("idDoc"),
                                        res.getNString("titreDoc"),
                                        res.getString("contenuDoc"),
                                        res.getDate("dateDoc"));
        return newDoc;
    }

    @Override
    public String toString(){
        return "Document: Id= "+this.getId()+", Titre: "+this.getTitre()+", Contenu: "+this.getContenu()+", Date: "+this.getDate();
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Document){
            return this.getId()==(((Document) o).getId())
                    && this.getTitre().equals(((Document) o).getTitre())
                    && this.getContenu().equals( ((Document) o).getContenu())
                    && this.getDate().equals(((Document) o).getDate());
        }
        return false;
    }

    @Override
    public int hashCode(){
        return Objects.hash(this.getId(),this.getTitre(),this.getContenu(),this.getDate());
    }
}
