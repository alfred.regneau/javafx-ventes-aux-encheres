package modeles;

import exceptions.IntrouvableException;
import interfaces.util.Util;
import javafx.scene.control.Alert;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Message
{
    private int id;
    private Date date;
    private String contenu;
    private Utilisateur destinataire;
    private Utilisateur expediteur;
    private boolean lu;

    public Message(int id, Date date, String contenu,boolean lu,Utilisateur destinataire,Utilisateur expediteur) {
        this.id = id;
        this.date = date;
        this.contenu = contenu;
        this.lu=lu;
        this.destinataire=destinataire;
        this.expediteur=expediteur;
    }

    public int getId()
    {
        return id;
    }

    public Date getDate()
    {
        return date;
    }

    public String getContenu()
    {
        return contenu;
    }

    public boolean isLu()
    {
        return lu;
    }

    public Utilisateur getDestinataire() {
        return destinataire;
    }

    public Utilisateur getExpediteur() {
        return expediteur;
    }

    public void setId(int id) { this.id = id; }

    @Override
    public String toString() {
        return "Message: Id= "+id+", Date: "+(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date))+", Contenu: "+contenu+", Destinataire= "+destinataire+",Expéditeur= "+expediteur+", Lu:"+lu;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message = (Message) o;
        return getId() == message.getId() &&
                isLu() == message.isLu() &&
                getDate().equals(message.getDate()) &&
                getContenu().equals(message.getContenu()) &&
                getDestinataire().equals(message.getDestinataire()) &&
                getExpediteur().equals(message.getExpediteur());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDate(), getContenu(), getDestinataire(), getExpediteur(), isLu());
    }

    public static Message lireMessage(ResultSet res) throws SQLException
    {
        Message newMessage= null;
        try
        {
            newMessage = new Message(res.getInt("idMsg"),
                                            new Date(res.getTimestamp("dateMsg").getTime()),
                                            res.getString("contenuMsg"),
                                            (res.getString("luMsg").charAt(0) == 'O'),
                                            Requetes.recupererUtilisateurParId(GestionnaireSQL.getConnexion(), res.getInt("idUt_dest")),
                                            Requetes.recupererUtilisateurParId(GestionnaireSQL.getConnexion(), res.getInt("idUt_exp")));
        }
        catch (IntrouvableException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Utilisateur introuvable", "Impossible de trouver un des utilisateurs lors de la lecture d'un message");
        }
        return newMessage;
    }


}
