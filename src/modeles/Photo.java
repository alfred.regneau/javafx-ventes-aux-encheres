package modeles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class Photo {

    private int id;
    private String titre;
    private byte[] img;


    public Photo( int id, String titre, byte[] img){
        this.id = id;
        this.titre = titre;
        this.img = img;
    }

    public int getId(){
        return this.id;
    }

    public String getTitre(){
        return this.titre;
    }

    public byte[] getImg(){
        return this.img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    @Override
    public String toString(){
        return "Photo: Id="+id+", Titre= "+titre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Photo)) return false;
        Photo photo = (Photo) o;
        return getId() == photo.getId() &&
                getTitre().equals(photo.getTitre());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitre());
    }

    public static Photo lirePhoto(ResultSet res) throws SQLException {

        Photo newPhoto = new Photo(res.getInt("idPh"),
                                  res.getString("titrePh"),
                                  res.getBytes("imgPh")
                                  );

        return newPhoto;
    }

}
