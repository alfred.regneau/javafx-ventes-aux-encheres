package modeles;

import modeles.sql.ConnexionMySQL;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class Vente {

    private int idVente;
    private Objet objet;
    private Double prixBase;
    private Double prixMin;
    private Date dateDebut;
    private Date dateFin;
    private Statut statutVente;


    public Vente(int id, Double prixBase, Double prixMin, Date dateDebut, Date dateFin, Objet objet, Statut statut) {
        this.idVente = id;
        this.prixBase = prixBase;
        this.prixMin = prixMin;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.statutVente = statut;
        this.objet = objet;
    }

    public int getId() {
        return idVente;
    }

    public Double getPrixBase() {
        return prixBase;
    }

    public Double getPrixMin() {
        return prixMin;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public Statut getStatut() {
        return statutVente;
    }

    public int getNbImage() {
        return 4;
    }

    public void retirerVente(Vente vente) {
        int i = 1;
    }

    public Objet getObjet() {
        return objet;
    }

    public void setIdVente(int idVente) { this.idVente = idVente; }

    public String toString() {
        return "Vente: Id= " + idVente + ", Prix de Base= "+prixBase+", Prix Minimum= "+prixMin+", Date de Début= "+dateDebut+", Date de Fin= "+dateFin+", Statut de la Vente= "+statutVente+", Objet= "+objet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vente)) return false;
        Vente vente = (Vente) o;
        return idVente == vente.idVente &&
                getObjet().equals(vente.getObjet()) &&
                getPrixBase().equals(vente.getPrixBase()) &&
                getPrixMin().equals(vente.getPrixMin()) &&
                getDateDebut().equals(vente.getDateDebut()) &&
                getDateFin().equals(vente.getDateFin()) &&
                statutVente.equals(vente.statutVente);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idVente, getObjet(), getPrixBase(), getPrixMin(), getDateDebut(), getDateFin(), statutVente);
    }

    public static Vente lireVente(ResultSet res) throws SQLException {
        Vente newVente = new Vente(res.getInt("idVe"),
                res.getDouble("prixBase"),
                res.getDouble("prixMin"),
                new Date(res.getTimestamp("debutVe").getTime()),
                new Date(res.getTimestamp("finVe").getTime()),
                Objet.lireObjet(res),
                Statut.lireStatut(res));

        return newVente;
    }

    public void setStatutVente(Statut statutVente)
    {
        this.statutVente = statutVente;
        try {
            Requetes.changerStatutVente(GestionnaireSQL.getConnexion(), this, this.statutVente);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}