package modeles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Utilisateur {
    private int id;
    private String pseudo;
    private String email;
    private String motDePasse;
    private boolean actif;
    private List<Objet> lObjet;
    private Role role;

    public Utilisateur(int id, String pseudo, String email, String motDePasse,boolean actif, Role role) {
        this.id = id;
        this.pseudo = pseudo;
        this.email = email;
        this.motDePasse = motDePasse;
        this.actif=actif;
        this.lObjet = new ArrayList<>();
        this.role=role;
    }

    public int getId() { return id; }

    public String getPseudo() { return pseudo; }

    public String getEmail() { return email; }

    public String getMotDePasse() { return motDePasse; }

    public List<Objet> getObjets() { return lObjet; }

    public Role getRole(){ return this.role; }

    public boolean isActif() { return actif; }

    public void setId(int id) { this.id = id; }

    public void setPseudo(String pseudo) { this.pseudo = pseudo; }

    public void setEmail(String email) { this.email = email; }

    public void setMotDePasse(String motDePasse) { this.motDePasse = motDePasse; }

    @Override
    public String toString(){
        return "Utilisateur: Id= "+id+", Pseudo= "+pseudo+",Email= "+email+", Mot de passe= "+motDePasse+", Actif= "+actif+", Liste des Objets= "+lObjet+", Role: "+role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilisateur)) return false;
        Utilisateur that = (Utilisateur) o;
        return getId() == that.getId() &&
                isActif() == that.isActif() &&
                getPseudo().equals(that.getPseudo()) &&
                getEmail().equals(that.getEmail()) &&
                getMotDePasse().equals(that.getMotDePasse()) &&
                Objects.equals(lObjet, that.lObjet) &&
                getRole().equals(that.getRole());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPseudo(), getEmail(), getMotDePasse(), isActif(), lObjet, getRole());
    }

    public void ajouteObjet(Objet o){ lObjet.add(o); }

    public void desactiver() { this.actif = false; }

    public void activer() { this.actif = true; }

    public static Utilisateur lireUtilisateur(ResultSet res) throws SQLException{
        Utilisateur newUtilisateur= new Utilisateur(res.getInt("idUt"),
                                                    res.getString("pseudoUt"),
                                                    res.getString("emailUt"),
                                                    res.getString("mdpUt"),
                                                    res.getBoolean("activeUt"),
                                                    Role.lireRole(res));
        return newUtilisateur;
    }
}
