package modeles.sql;

import exceptions.*;
import modeles.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Requetes
{
    public static List<Objet> recupererObjetsUtilisateur(ConnexionMySQL connexion, int idUtilisateur) throws SQLException
    {
        List<Objet> laListe = new ArrayList<>();

        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select * from OBJET natural join CATEGORIE where idUt=" + idUtilisateur);

        while (res.next())
        {
            laListe.add(Objet.lireObjet(res));
        }

        res.close();
        statement.close();
        return laListe;
    }

    /**
     * Ne retourne pas les objets dont la vente est prévue
     */
    public static List<Objet> recupererObjetsNonEnVente(ConnexionMySQL connexion, int idUtilisateur) throws SQLException
    {
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select * " +
                "from OBJET natural join CATEGORIE " +
                "where idUt=" + idUtilisateur + " " +
                "and idOb not in (" +
                "  select idOb " +
                "  from OBJET natural join VENTE " +
                "  where idSt=1 " +
                "  or idSt=2 " +
                "  or idSt=3 " +
                ")");

        List<Objet> objets = new ArrayList<>();

        while (res.next())
        {
            objets.add(Objet.lireObjet(res));
        }

        res.close();
        statement.close();

        return objets;
    }

    /* TODO bien plus tard

    dans mes objets : ajouter objets achetés (le faire ds mettre en vente aussi)
    qd on met en vente un objet acheté
    => copier l'objet dans la bd et changer le propriétaire (penser à copier les photos)

    ATTENTION : ne pas permettre l'édition de l'objet (ou créer une copie)

    Les objets vendus d'un utilisateur ne peuvent pas être remis en vente ? (ou osef puisqu'il peut très bien en vendre deux ?)

     */

    /**
     * @return l'id du nouvel objet
     */
    public static int copierObjet(ConnexionMySQL connexion, int idObjet, int idNvProprietaire) throws IntrouvableException, SQLException
    {
        Objet objet = recupererObjetParId(connexion, idObjet);
        int idObj = maxIdentifiantObjet(connexion) + 1;

        PreparedStatement statementObjet = connexion.prepareStatement("insert into OBJET values (?, ?, ?, ?, ?)");
        statementObjet.setInt(1, idObj);
        statementObjet.setString(2, objet.getNom());
        statementObjet.setString(3, objet.getDescription());
        statementObjet.setInt(4, objet.getCategorie().getIdCat());
        statementObjet.setInt(5, idNvProprietaire);
        statementObjet.executeUpdate();
        statementObjet.close();

        for (Photo photo : objet.getPhotos())
        {
            PreparedStatement statementPhoto = connexion.prepareStatement("insert into PHOTO values(?, ?, ?)");
            statementPhoto.setInt(1, maxIdentifiantPhoto(connexion) + 1);
            statementPhoto.setString(2, photo.getTitre());
            statementPhoto.setBytes(3, photo.getImg());
            statementPhoto.setInt(4, idObj);
            statementPhoto.executeUpdate();
            statementPhoto.close();
        }

        return idObj;
    }

    public static Objet recupererObjetParId(ConnexionMySQL connexion, int idObjet) throws SQLException, IntrouvableException
    {
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select * from OBJET natural join CATEGORIE where idOb=" + idObjet);

        if (res.next())
        {
            Objet obj = Objet.lireObjet(res);
            res.close();
            statement.close();

            return obj;
        }

        res.close();
        statement.close();

        throw new IntrouvableException();
    }

    public static Utilisateur recupererUtilisateurParId(ConnexionMySQL connexion, int id) throws SQLException, IntrouvableException
    {
        Statement statement = connexion.createStatement();
        ResultSet set = statement.executeQuery("select * from UTILISATEUR natural join ROLE where idUt=" + id);
        Utilisateur ut = null;

        if (set.next())
        {
            ut = Utilisateur.lireUtilisateur(set);
        }
        else
        {
            throw new IntrouvableException();
        }

        set.close();
        statement.close();

        return ut;
    }

    public static Utilisateur recupererUtilisateurPasPseudo(ConnexionMySQL connexion, String pseudo) throws SQLException, IntrouvableException
    {
        Statement statement = connexion.createStatement();
        ResultSet set = statement.executeQuery("select * from UTILISATEUR natural join ROLE where pseudoUt=" + pseudo);
        Utilisateur ut = null;

        if (set.next())
        {
            ut = Utilisateur.lireUtilisateur(set);
        }
        else
        {
            throw new IntrouvableException();
        }
        set.close();
        statement.close();

        return ut;
    }

    public static Categorie recupererCategorieParId(ConnexionMySQL connexion, int id) throws SQLException, IntrouvableException
    {
        Categorie cat = null;

        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select * from CATEGORIE where idCat='" + id + "'");

        if (res.next())
            cat = Categorie.lireCategorie(res);

        res.close();
        statement.close();

        if (cat == null)
            throw new IntrouvableException();

        return cat;
    }

    public static List<Categorie> recupererCategories(ConnexionMySQL connexion) throws SQLException
    {
        List<Categorie> liste = new ArrayList<>();

        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select * from CATEGORIE order by idCat");

        while (res.next())
        {
            liste.add(Categorie.lireCategorie(res));
        }

        res.close();
        statement.close();

        return liste;
    }

    public static int connexionUtilisateur(ConnexionMySQL connexion, String nomUt, String mdp) throws SQLException, IntrouvableException, MotDePasseIncorrectException
    {
        int idUt = 0;
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select * from UTILISATEUR where pseudoUt='" + nomUt + "'");

        if (!res.next())
            throw new IntrouvableException();

        if (!res.getString("mdpUt").equals(mdp))
            throw new MotDePasseIncorrectException();
        idUt = res.getInt("idUt");

        res.close();
        statement.close();
        return idUt;
    }

    public static List<Vente> recupererVenteParOrdreDeFin(ConnexionMySQL connexion) throws SQLException
    {

        List<Vente> listeVente = new ArrayList<>();
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select * " +
                "from VENTE natural join OBJET natural join CATEGORIE natural join STATUT " +
                "order by finVe");

        while (res.next())
        {
            listeVente.add(Vente.lireVente(res));
        }
        res.close();
        statement.close();
        return listeVente;
    }

    public static List<Vente> recupererVentesUtilisateur(ConnexionMySQL connexion, int idUtilisateur) throws SQLException
    {

        List<Vente> listeVente = new ArrayList<>();
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select * " +
                "from VENTE natural join UTILISATEUR natural join OBJET natural join CATEGORIE natural join STATUT " +
                "where idUt='" + idUtilisateur + "'" +
                "order by finVe");
        while (res.next())
        {
            listeVente.add(Vente.lireVente(res));
        }
        res.close();
        statement.close();

        return listeVente;
    }

    public static List<Vente> recupererVentesEnCours(ConnexionMySQL connexion) throws SQLException
    {
        List<Vente> listeVente = new ArrayList<>();
        Statement s = connexion.createStatement();
        ResultSet rs = s.executeQuery("select * " +
                "from VENTE natural join STATUT natural join OBJET natural join CATEGORIE " +
                "where debutVe <= NOW() and finVe >= NOW() and idSt <= 3");

        while(rs.next())
        {
            listeVente.add(Vente.lireVente(rs));
        }
        rs.close();
        s.close();

        return listeVente;
    }

    public static List<Conversation> recupererConversationsUtilisateur(ConnexionMySQL connexion, int idUtilisateur) throws SQLException, IntrouvableException
    {
        PreparedStatement statement = connexion.prepareStatement("select * " +
                "from MESSAGE natural join OBJET natural join CATEGORIE " +
                "where idUt_dest=? or " +
                "idUt_exp=? " +
                "order by dateMsg desc");
        statement.setInt(1, idUtilisateur);
        statement.setInt(2, idUtilisateur);
        ResultSet res = statement.executeQuery();

        List<Conversation> conversations = new ArrayList<>();
        Utilisateur utilisateurLocal;

        try
        {
            utilisateurLocal = recupererUtilisateurParId(connexion, idUtilisateur);
        }
        catch (IntrouvableException e)
        {
            e.printStackTrace();
            System.out.println("Impossible de récupérer l'utilisateur concerné par la requête recupérer conversations");
            throw new IntrouvableException();
        }

        while (res.next())
        {
            Message msg = Message.lireMessage(res);
            Utilisateur correspondant = (msg.getDestinataire().equals(utilisateurLocal) ? msg.getExpediteur() : msg.getDestinataire());
            Objet objetConcerne = Objet.lireObjet(res);
            Conversation conversationCourante = null;

            for (Conversation conv : conversations)
            {
                if (conv.getCorrespondant().equals(correspondant) && conv.getObjetConcerne().equals(objetConcerne))
                {
                    conversationCourante = conv;
                    break;
                }
            }

            if (conversationCourante == null)
            {
                conversationCourante = new Conversation(new ArrayList<>(), objetConcerne, correspondant);
                conversations.add(conversationCourante);
            }

            conversationCourante.ajouterMessage(msg);
        }

        for (Conversation c : conversations)
            c.trier();

        res.close();
        statement.close();

        return conversations;
    }

    public static int recupererNbMessagesNonLus(ConnexionMySQL connexion, int idUtilisateur) throws SQLException, IntrouvableException
    {
        Statement statement = connexion.createStatement();
        ResultSet rset = statement.executeQuery("select count(*) from MESSAGE where idUt_dest=" + idUtilisateur + " and luMsg='N'");

        if (rset.next())
        {
            String nb = rset.getString(1);
            rset.close();
            statement.close();

            return Integer.parseInt(nb);
        }

        rset.close();
        statement.close();

        throw new IntrouvableException();
    }

    public static void marquerConversationCommeLue(ConnexionMySQL connexion, Conversation conv) throws SQLException
    {
        List<Message> messagesNonLus = new ArrayList<>();

        for(Message msg : conv.getMessages())
        {
            // message non lu ET expédié par le correspondant
            if(!msg.isLu() && msg.getDestinataire().getId() == ConnexionUtilisateur.getIdUtilisateurConnecte())
            {
                messagesNonLus.add(msg);
            }
        }

        if(messagesNonLus.size() == 0)
            return;

        String requete = "update MESSAGE set luMsg='O' where ";

        for(Message msg : messagesNonLus)
            requete += "idMsg=" + msg.getId() + " or ";

        requete = requete.substring(0, requete.length() - 4);
        requete += ";";

        Statement statement = connexion.createStatement();
        statement.executeUpdate(requete);
        statement.close();
    }

    public static int maxIdentifiantUtilisateur(ConnexionMySQL connexion) throws SQLException
    {
        int maxIdentifiant = 0;
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select max(idUt) from UTILISATEUR");
        if (res.next())
        {
            maxIdentifiant = res.getInt(1);
        }
        res.close();
        statement.close();
        return maxIdentifiant;
    }

    public static int maxIdentifiantObjet(ConnexionMySQL connexion) throws SQLException
    {
        int maxIdentifiant = 0;
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select max(idOb) from OBJET");
        if (res.next())
            maxIdentifiant = res.getInt(1);

        res.close();
        statement.close();
        return maxIdentifiant;
    }

    public static int maxIdentifiantPhoto(ConnexionMySQL connexion) throws SQLException
    {
        int maxIdentifiant = 0;
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select max(idPh) from PHOTO");
        if (res.next())
            maxIdentifiant = res.getInt(1);

        res.close();
        statement.close();
        return maxIdentifiant;
    }


    public static boolean nomUtilisateurExiste(ConnexionMySQL connexion, String nom) throws SQLException
    {
        Statement s = connexion.createStatement();
        ResultSet res = s.executeQuery("select count(*) from UTILISATEUR where pseudoUt='" + nom + "'");
        res.next();

        boolean reponse = res.getInt(1) != 0;

        res.close();
        s.close();

        return reponse;
    }

    public static boolean emailUtilisateurExiste(ConnexionMySQL connexion, String email) throws SQLException
    {
        Statement s = connexion.createStatement();
        ResultSet res = s.executeQuery("select count(*) from UTILISATEUR where emailUt='" + email + "'");
        res.next();

        boolean reponse = res.getInt(1) != 0;

        res.close();
        s.close();

        return reponse;
    }

    public static void ajouterUtilisateur(ConnexionMySQL connexion, Utilisateur user) throws SQLException, PseudoDejaPrisException, EmailDejaPrisException
    {
        if (nomUtilisateurExiste(connexion, user.getPseudo()))
            throw new PseudoDejaPrisException();
        if (emailUtilisateurExiste(connexion, user.getEmail()))
            throw new EmailDejaPrisException();

        PreparedStatement ps = connexion.prepareStatement("insert into UTILISATEUR values(?,?,?,?,?,?)");
        user.setId(maxIdentifiantUtilisateur(connexion) + 1);
        ps.setInt(1, user.getId());
        ps.setString(2, user.getPseudo());
        ps.setString(3, user.getEmail());
        ps.setString(4, user.getMotDePasse());
        ps.setBoolean(5, user.isActif());
        ps.setInt(6, user.getRole().getId());
        ps.executeUpdate();
        ps.close();
    }

    public static boolean objetExiste(ConnexionMySQL connexion, int id) throws SQLException
    {
        Statement s = connexion.createStatement();
        ResultSet rs = s.executeQuery("select * from OBJET where idOb = " + id);
        boolean res = false;

        if (rs.next())
            res = true;

        rs.close();
        s.close();

        return res;
    }

    public static void ajouterObjet(ConnexionMySQL connexion, Objet objet) throws SQLException
    {
        PreparedStatement ps = connexion.prepareStatement("insert into OBJET values (?, ?, ?, ?, ?)");
        ps.setInt(1, objet.getId());
        ps.setString(2, objet.getNom());
        ps.setString(3, objet.getDescription());
        ps.setInt(4, objet.getCategorie().getIdCat());
        ps.setInt(5, ConnexionUtilisateur.getIdUtilisateurConnecte());

        ps.executeUpdate();

        ps.close();

        modifierPhotosObjet(connexion, objet);
    }

    public static void modifierObjet(ConnexionMySQL connexion, Objet objet, List<Integer> idsSuppr) throws SQLException
    {
        PreparedStatement ps = connexion.prepareStatement("update OBJET set nomOb = ?, descriptionOb = ?, idCat = ?, idUt = ? where idOb = ?");
        ps.setString(1, objet.getNom());
        ps.setString(2, objet.getDescription());
        ps.setInt(3, objet.getCategorie().getIdCat());
        ps.setInt(4, ConnexionUtilisateur.getIdUtilisateurConnecte());
        ps.setInt(5, objet.getId());

        ps.executeUpdate();

        ps.close();

        modifierPhotosObjet(connexion, objet);
        for (Integer id : idsSuppr)
            supprimerPhoto(connexion, id);
    }

    public static void modifierPhotosObjet(ConnexionMySQL connexion, Objet objet) throws SQLException
    {
        PreparedStatement ps = connexion.prepareStatement("update PHOTO set imgPh = ? where idPh = ?");
        PreparedStatement psAdd = connexion.prepareStatement("insert into PHOTO values(?, ?, ?, ?)");
        psAdd.setInt(4, objet.getId());
        for (Photo p : objet.getPhotos())
        {
            if (p.getId() <= maxIdentifiantPhoto(connexion))
            {
                System.out.println("update");
                ps.setBytes(1, p.getImg());
                ps.setInt(2, p.getId());
                ps.executeUpdate();
            }

            else
            {
                System.out.println("add");
                psAdd.setInt(1, p.getId());
                psAdd.setString(2, p.getTitre());
                psAdd.setBytes(3, p.getImg());
                psAdd.executeUpdate();
            }
        }

        ps.close();
        psAdd.close();
    }

    public static void supprimerPhoto(ConnexionMySQL connexion, int id) throws SQLException
    {
        Statement s = connexion.createStatement();
        s.executeUpdate("delete from PHOTO where idPh = " + id);

        s.close();
    }

    public static Utilisateur recupererUtilisateurParIdObjet(ConnexionMySQL connexion, int idObjet) throws SQLException, IntrouvableException{
        Statement s = connexion.createStatement();
        ResultSet rs = s.executeQuery("select idUt from OBJET natural join CATEGORIE where idOb = " + idObjet);

        rs.next();
        Utilisateur user = recupererUtilisateurParId(connexion, rs.getInt(1));

        rs.close();
        s.close();

        return user;

        /*ConnexionMySQL conn = GestionnaireSQL.getConnexion();
        int util = ConnexionUtilisateur.getIdUtilisateurConnecte();
        try {
            List<Objet> liste = recupererObjetsUtilisateur(conn, util);
            for (Objet o : liste)
                if (o.getId() == idObjet)
                    return recupererUtilisateurParId(conn, util);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrouvableException e) {
            return null;
        }

        res.close();
        statement.close();

        throw new IntrouvableException();*/
    }

    public static Utilisateur recupererProprietaireVente(ConnexionMySQL connexion, int idVente) throws SQLException, IntrouvableException
    {
        Utilisateur utilisateur;

        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select * " +
                "from VENTE natural join OBJET natural join UTILISATEUR natural join ROLE " +
                "where idVe=" + idVente);

        if (res.next())
        {
            utilisateur = Utilisateur.lireUtilisateur(res);
            res.close();
            statement.close();

            return utilisateur;
        }

        res.close();
        statement.close();

        throw new IntrouvableException();
    }

    public static int maxIdentifiantMessage(ConnexionMySQL connexion) throws SQLException
    {
        int maxIdentifiant = 0;
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select max(idMsg) from MESSAGE");
        if (res.next())
            maxIdentifiant = res.getInt(1);

        res.close();
        statement.close();
        return maxIdentifiant;
    }

    public static int maxIdentifiantVente(ConnexionMySQL connexion) throws SQLException
    {
        int maxIdentifiant = 0;
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select max(idVe) from VENTE");
        if (res.next())
            maxIdentifiant = res.getInt(1);

        res.close();
        statement.close();
        return maxIdentifiant;
    }

    public static void ajouterNouveauMessage(ConnexionMySQL connexion, Conversation conv, Message message) throws SQLException
    {
        PreparedStatement ps = connexion.prepareStatement("insert into MESSAGE values(?,?,?,?,?,?,?)");
        message.setId(maxIdentifiantMessage(connexion) + 1);
        ps.setInt(1, message.getId());
        ps.setTimestamp(2, new java.sql.Timestamp(message.getDate().getTime()));
        ps.setString(3, message.getContenu());
        ps.setString(4, message.isLu() ? "0" : "N");
        ps.setInt(5, conv.getObjetConcerne().getId());
        ps.setInt(6, message.getExpediteur().getId());
        ps.setInt(7, message.getDestinataire().getId());
        ps.executeUpdate();
        ps.close();
    }

    public static void ajouterNouvelleVente(ConnexionMySQL connexion, Vente vente) throws SQLException
    {
       PreparedStatement ps = connexion.prepareStatement("insert into VENTE values(?,?,?,?,?,?,?)");
        vente.setIdVente(maxIdentifiantVente(connexion)+1);
        ps.setInt(1,vente.getId());
        ps.setDouble(2,vente.getPrixBase());
        ps.setDouble(3,vente.getPrixMin());
        ps.setTimestamp(4,new java.sql.Timestamp(vente.getDateDebut().getTime()));
        ps.setTimestamp(5,new java.sql.Timestamp(vente.getDateFin().getTime()));
        ps.setString(6, String.valueOf(vente.getStatut().getIdStatut()));
        ps.setInt(7,vente.getObjet().getId());
        ps.executeUpdate();
        ps.close();

    }

    public static Proposition recupererPropositionMaximumPourVente(ConnexionMySQL connexion, int idVente) throws SQLException, ResultatVideException
    {
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select * " +
                "from ENCHERIR natural join UTILISATEUR natural join ROLE natural join VENTE inner join OBJET on VENTE.idOb = OBJET.idOb natural join CATEGORIE natural join STATUT " +
                "where VENTE.idVe=" + idVente + " " +
                "and ENCHERIR.montant=(" +
                "   select max(montant) " +
                "   from ENCHERIR " +
                "   where idVe=" + idVente +
                ")");

        if(res.next())
        {
            Proposition prop = Proposition.lireProposition(res);
            res.close();
            statement.close();

            return prop;
        }

        res.close();
        statement.close();

        throw new ResultatVideException();
    }

    public static void ajouterProposition(ConnexionMySQL connexion, int idVente, int idUt, double montant, Date dateHeure) throws SQLException
    {
        PreparedStatement statement = connexion.prepareStatement("insert into ENCHERIR values (?, ?, ?, ?)");
        statement.setInt(1, idUt);
        statement.setInt(2, idVente);
        statement.setTimestamp(3, new Timestamp(dateHeure.getTime()));
        statement.setDouble(4, montant);
        statement.executeUpdate();

        statement.close();
    }

    public static List<Proposition> recupererEncheresUtilisateur(ConnexionMySQL connexion, int idUtilisateur) throws SQLException
    {
        List<Proposition> res = new ArrayList<>();
        boolean add;
        int modif = -1;
        Proposition tmp;
        Statement s = connexion.createStatement();
        ResultSet rs = s.executeQuery("select * from ENCHERIR natural join UTILISATEUR natural join ROLE natural join VENTE join OBJET natural join CATEGORIE natural join STATUT on VENTE.idOb = OBJET.idOb where ENCHERIR.idUt = " + idUtilisateur + " and VENTE.debutVe <= NOW() and VENTE.finVe >= NOW()");
        while(rs.next())
        {
            add = true;
            tmp = Proposition.lireProposition(rs);
            if (res.isEmpty())
                res.add(tmp);
            else
                {
                    for (int i = 0; i < res.size(); i++)
                    {
                        if (tmp.getVente().getId() == res.get(i).getVente().getId())
                        {
                            add = false;
                            if(tmp.getMontant() >= res.get(i).getMontant())
                                modif = i;
                        }
                    }

                    if (add)
                        res.add(tmp);
                    else if (modif > -1)
                        res.get(modif).setProposition(tmp);
                }
        }

        rs.close();
        s.close();
        return res;
    }

    public static List<Vente> rechercheVente(ConnexionMySQL connexion, String recherche) throws SQLException
    {
        List<Vente> res = new ArrayList<>();
        Statement s = connexion.createStatement();
        ResultSet rs = s.executeQuery("select * from VENTE natural join OBJET natural join CATEGORIE natural join STATUT where nomOb like '%" + recherche + "%' and debutVe <= NOW() and finVe >= NOW() and idSt <= 3");
        while(rs.next())
            res.add(Vente.lireVente(rs));

        rs.close();
        s.close();

        return res;
    }

    public static List<Proposition> rechercheProposition(ConnexionMySQL connexion, String recherche, int idUtilisateur) throws SQLException
    {
        List<Proposition> res = new ArrayList<>();
        Statement s = connexion.createStatement();
        ResultSet rs = s.executeQuery("select * from ENCHERIR natural join UTILISATEUR natural join ROLE natural join STATUT natural join VENTE join OBJET natural join CATEGORIE on VENTE.idOb = OBJET.idOb where ENCHERIR.idUt = " + idUtilisateur + " and nomOb like '%" + recherche + "%'");
        while(rs.next())
            res.add(Proposition.lireProposition(rs));

        rs.close();
        s.close();

        return res;
    }

    public static List<Objet> rechercheObjetUtilisateur(ConnexionMySQL connexion, String recherche, int idUtilisateur) throws SQLException
    {
        List<Objet> res = new ArrayList<>();
        Statement s = connexion.createStatement();
        ResultSet rs = s.executeQuery("select * from OBJET natural join CATEGORIE where idUt = " + idUtilisateur + " and nomOb like '%" + recherche + "%'");
        while(rs.next())
            res.add(Objet.lireObjet(rs));

        rs.close();
        s.close();

        return res;
    }

    public static List<Vente> recupererVentesTerminantDansLes24H(ConnexionMySQL connexion) throws SQLException
    {
        List<Vente> res = new ArrayList<>();
        Statement s = connexion.createStatement();
        ResultSet rs = s.executeQuery("select * from VENTE natural join STATUT natural join OBJET natural join CATEGORIE where TIMESTAMPDIFF(HOUR, NOW(), finVe) <= 24 and TIMESTAMPDIFF(HOUR, NOW(), finVe) >= 0");

        while(rs.next())
            res.add(Vente.lireVente(rs));
        rs.close();
        s.close();
        return res;
    }

    public static void changerStatutVente(ConnexionMySQL connexion, Vente vente, Statut statut) throws SQLException
    {

        Statement statement = connexion.createStatement();
        statement.executeUpdate("update VENTE set idSt = " + statut.getIdStatut() + " where idVe = " + vente.getId());
        statement.close();
    }

    public static Proposition recupererPropositionUtilisateur(ConnexionMySQL connexion, int idVente, int idUtilisateur) throws SQLException, ResultatVideException
    {
        Proposition p = null;
        Proposition tmp = null;

        Statement s = connexion.createStatement();
        ResultSet rs = s.executeQuery("select * from ENCHERIR natural join UTILISATEUR natural join ROLE natural join VENTE join OBJET natural join CATEGORIE natural join STATUT on VENTE.idOb = OBJET.idOb where ENCHERIR.idUt = " + idUtilisateur + " and VENTE.idVe = " + idVente);
        while(rs.next())
        {
            tmp = Proposition.lireProposition(rs);
            if(p == null)
                p = tmp;

            else
            {
                if(tmp.getMontant() > p.getMontant())
                    p = tmp;
            }
        }

        rs.close();
        s.close();

        if(p == null)
            throw new ResultatVideException();

        return p;
    }

    public static List<Vente> recupererVentesUtilisateurEnCours(ConnexionMySQL connexion, int idUtilisateur) throws SQLException
    {

        List<Vente> listeVente = new ArrayList<>();
        Statement statement = connexion.createStatement();
        ResultSet res = statement.executeQuery("select * " +
                "from VENTE natural join UTILISATEUR natural join OBJET natural join CATEGORIE natural join STATUT " +
                "where idUt='" + idUtilisateur + "'" + "and idSt = 2 " +
                "order by finVe");
        while (res.next())
        {
            listeVente.add(Vente.lireVente(res));
        }
        res.close();
        statement.close();

        return listeVente;
    }
}
