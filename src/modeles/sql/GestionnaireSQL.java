package modeles.sql;

import java.sql.SQLException;

public class GestionnaireSQL
{
    private static ConnexionMySQL connexion;

    public static ConnexionMySQL getConnexion()
    {
        return connexion;
    }

    public static void init() throws ClassNotFoundException, SQLException
    {
        connexion = new ConnexionMySQL();
        connexion.connecter("servinfo-mariadb", "DBgalle", "galle", "galle");
    }

    public static void fin() throws SQLException
    {
        connexion.close();
    }
}
