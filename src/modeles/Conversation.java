package modeles;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Toujours relative à l'utilisateur connecté
 */
public class Conversation
{
    private List<Message> messages;
    private Objet objetConcerne;
    private Utilisateur correspondant;

    /**
     *
     * @param messages
     * @param objetConcerne
     * @param correspondant celui avec lequel l'utilisateur connecté discute
     */
    public Conversation(List<Message> messages, Objet objetConcerne, Utilisateur correspondant)
    {
        this.messages = messages;
        this.objetConcerne = objetConcerne;
        this.correspondant = correspondant;
    }

    public List<Message> getMessages()
    {
        return this.messages;
    }

    public Objet getObjetConcerne()
    {
        return this.objetConcerne;
    }

    public Utilisateur getCorrespondant()
    {
        return this.correspondant;
    }

    @Override
    public String toString()
    {
        String res = "";

        res += "=======\n";
        res += "Avec : " + this.getCorrespondant().getPseudo() + "\n";
        res += "Concernant : " + this.getObjetConcerne().getNom() + "\n";
        res += "Messages :\n";

        for(Message m : this.getMessages())
        {
            res += "["+ new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(m.getDate()) +"] [" + m.getExpediteur().getPseudo() + " => " + m.getDestinataire().getPseudo() + "] [" + (m.isLu() ? "LU" : "NON-LU") + "] " + m.getContenu() + "\n";
        }

        return res;
    }

    @Override
    public boolean equals(Object o)
    {
        if(o instanceof Conversation)
        {
            Conversation c = (Conversation)o;
            return c.getObjetConcerne().equals(objetConcerne) && c.getCorrespondant().equals(correspondant)
                    && c.getMessages().equals(messages);
        }

        return false;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(objetConcerne, correspondant, messages);
    }

    /**
     * Ne met pas à jour la bd
     * @param message
     */
    public void ajouterMessage(Message message)
    {
        this.messages.add(message);
    }

    /**
     * Ne met pas à jour la bd
     * @param message
     */
    public void supprimerMessage(Message message)
    {
        this.messages.remove(message);
    }

    /**
     * Trie les messages par ordre chronologique
     */
    public void trier()
    {
        Collections.sort(this.messages,
                new Comparator<Message>()
                {
                    @Override
                    public int compare(Message m1, Message m2)
                    {
                        return m1.getDate().compareTo(m2.getDate());
                    }
                });
    }

    /**
     * Retourne la date du dernier message DE LA LISTE
     * @return
     */
    public Date getDateDernierMessage()
    {
        if(this.messages.size() > 0)
            return this.messages.get(this.messages.size() - 1).getDate();
        return new Date(0);
    }
}
