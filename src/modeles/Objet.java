package modeles;

import modeles.sql.ConnexionMySQL;
import modeles.sql.GestionnaireSQL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Objet
{
    private int idObjet;
    private String nom;
    private String description;
    private List<Photo> photos;
    private boolean enVente;
    private Categorie ctgr;

    public Objet(int id, String nom, String description, Categorie ctgr)
    {
        this.idObjet = id;
        this.nom = nom;
        this.description = description;
        this.photos = new ArrayList<>();
        this.enVente = false;
        this.ctgr = ctgr;

        lirePhotos();
    }

    public int getId()
    {
        return idObjet;
    }

    public String getNom()
    {
        return nom;
    }

    public String getDescription()
    {
        return description;
    }

    public List<Photo> getPhotos()
    {
        return photos;
    }

    public Categorie getCategorie()
    {
        return this.ctgr;
    }

    public boolean enVente()
    {
        return this.enVente;
    }

    public void lirePhotos()
    {
        ConnexionMySQL conn = GestionnaireSQL.getConnexion();

        try
        {
            Statement s = conn.createStatement();
            ResultSet res = s.executeQuery("select * from PHOTO where idOb=" + this.idObjet);

            while(res.next())
            {
                this.photos.add(Photo.lirePhoto(res));
            }

            res.close();
            s.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }


    }

    public void setCtgr(Categorie ctgr) {
        this.ctgr = ctgr;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public void ajouterPhoto(Photo photo)
    {
        this.photos.add(photo);
    }

    public void supprimerPhoto(int i)
    {
        this.photos.remove(i);
    }

    @Override
    public String toString()
    {
        return getNom();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Objet)) return false;
        Objet objet = (Objet) o;
        return idObjet == objet.idObjet &&
                enVente == objet.enVente &&
                Objects.equals(getNom(), objet.getNom()) &&
                Objects.equals(getDescription(), objet.getDescription()) &&
                Objects.equals(getPhotos(), objet.getPhotos()) &&
                Objects.equals(ctgr, objet.ctgr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idObjet, getNom(), getDescription(), getPhotos(), enVente, ctgr);
    }

    public static Objet lireObjet(ResultSet res) throws SQLException
    {
        return lireObjet(res, Categorie.lireCategorie(res));
    }

    public static Objet lireObjet(ResultSet res, Categorie cat) throws SQLException
    {
        return new Objet(res.getInt("idOb"),
                         res.getString("nomOb"),
                         res.getString("descriptionOb"),
                         cat);
    }
}
