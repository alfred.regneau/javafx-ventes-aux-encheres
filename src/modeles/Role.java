package modeles;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

public class Role {
    private int id;
    private String nom;

    public Role(int id, String nom){
        this.id=id;
        this.nom= nom;
    }

    public Integer getId(){
        return this.id;
    }

    public String getNom(){
        return this.nom;
    }

    public static Role lireRole(ResultSet res) throws SQLException{
        /*Statement stat= laConnexion.createStatement();
        ResultSet res=stat.executeQuery("select idRole,nomRole from ROLE natural join UTILISATEUR where idUt="+iduser);
        Role role=null;
        if(res.next()==false){
            throw new SQLException("Role non trouvé");
        }
        return new Role(res.getInt("idRole"),res.getString("nomRole")); */

        Role newRole = new Role(res.getInt("idRole"),
                                res.getString("nomRole"));
        return newRole;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getId(),this.getNom());
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Role){
            return this.getId()==((Role) o).getId() && this.getNom().equals(((Role) o).getNom());
        }
        return false;
    }

    @Override
    public String toString() {
        return "Role: Id= "+this.getId()+", Nom= "+this.getNom();
    }
}
