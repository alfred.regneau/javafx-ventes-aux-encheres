package modeles;

import exceptions.IntrouvableException;
import exceptions.MotDePasseIncorrectException;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;

public class ConnexionUtilisateur
{
    private static int idUtilisateurConnecte;
    private static boolean estConnecte;

    public static void connexion(String nomUtilisateur, String motDePasseUtilisateur) throws IntrouvableException, MotDePasseIncorrectException, SQLException
    {
        idUtilisateurConnecte = Requetes.connexionUtilisateur(GestionnaireSQL.getConnexion(), nomUtilisateur, motDePasseUtilisateur);
        estConnecte = true;
    }

    public static void deconnexion()
    {
        idUtilisateurConnecte = -1;
        estConnecte = false;
    }

    public static int getIdUtilisateurConnecte()
    {
        return idUtilisateurConnecte;
    }

    public static boolean estConnecte()
    {
        return estConnecte;
    }
}
