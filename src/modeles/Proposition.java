package modeles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Objects;

public class Proposition
{
    private Utilisateur user;
    private Vente vente;
    private Date dateHeureEnchere;
    private double montant;

    public Proposition(Utilisateur user, Vente vente, Date date, double montant)
    {
        this.user = user;
        this.vente = vente;
        this.dateHeureEnchere = date;
        this.montant = montant;
    }

    public void setProposition(Proposition p)
    {
        this.user = p.user;
        this.vente = p.vente;
        this.dateHeureEnchere = p.dateHeureEnchere;
        this.montant = p.montant;
    }

    public Utilisateur getUtilisateur()
    {
        return this.user;
    }

    public Vente getVente()
    {
        return this.vente;
    }

    public Date getDateHeureEnchere()
    {
        return this.dateHeureEnchere;
    }

    public double getMontant()
    {
        return this.montant;
    }

    @Override
    public String toString(){
        return "Proposition: Utilisateur= "+user+", Vente= "+vente+", Date="+dateHeureEnchere+", Montant= "+montant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Proposition)) return false;
        Proposition that = (Proposition) o;
        return Double.compare(that.getMontant(), getMontant()) == 0 &&
                user.equals(that.user) &&
                getVente().equals(that.getVente()) &&
                getDateHeureEnchere().equals(that.getDateHeureEnchere());
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, getVente(), getDateHeureEnchere(), getMontant());
    }

    /**
     * Lit à partir d'un ResultSet contenant toutes les informations (Utilisateur, Vente, Encherir) (jointure)
     * @param rset
     * @return
     * @throws SQLException
     */
    public static Proposition lireProposition(ResultSet rset) throws SQLException
    {
        return lireProposition(Utilisateur.lireUtilisateur(rset),
                                Vente.lireVente(rset),
                                rset);
    }

    /**
     * Lit à partir des objets préchargés et d'un resultset contenant les informations venant de la table Encherir
     * @param user
     * @param vente
     * @param rset
     * @return
     * @throws SQLException
     */
    public static Proposition lireProposition(Utilisateur user, Vente vente, ResultSet rset) throws SQLException
    {
        return new Proposition(user, vente, rset.getDate("dateheure"), rset.getDouble("montant"));
    }
}
