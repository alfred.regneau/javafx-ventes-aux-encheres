package modeles;

import interfaces.util.Util;
import javafx.scene.control.Alert;
import modeles.sql.ConnexionMySQL;
import modeles.sql.Requetes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

public class Categorie
{
    private int id;
    private String nom;

    public Categorie(int id, String nom)
    {
        this.id = id;
        this.nom = nom;
    }

    public Integer getIdCat()
    {
        return this.id;
    }

    public String getNomCat()
    {
        return this.nom;
    }



    public static Categorie lireCategorie(ResultSet res) throws SQLException
    {
        Categorie newCategorie = new Categorie(res.getInt("idCat"),
                res.getString("nomCat"));
        return newCategorie;
    }
    @Override
    public String toString()
    {
        return getNomCat();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Categorie)) return false;
        Categorie categorie = (Categorie) o;
        return id == categorie.id &&
                nom.equals(categorie.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom);
    }
}
