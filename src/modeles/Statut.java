package modeles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class Statut {
    private int idStatut;
    private String nomStatut;

    public Statut(int idStatut, String nomStatut) {
        this.idStatut = idStatut;
        this.nomStatut = nomStatut;
    }

    public int getIdStatut() {
        return idStatut;
    }

    public String getNomStatut() {
        return nomStatut;
    }

    public void setStatut(int numStatut){
        int i=numStatut;
    }

    @Override
    public String toString(){
        return "Statut: Id= "+idStatut+", Nom Statut= "+nomStatut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Statut)) return false;
        Statut statut = (Statut) o;
        return getIdStatut() == statut.getIdStatut() &&
                getNomStatut().equals(statut.getNomStatut());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdStatut(), getNomStatut());
    }

    public static Statut lireStatut(ResultSet res) throws SQLException {
        Statut newStatut = new Statut ( res.getInt("idSt"), res.getString("nomSt"));

        return newStatut;
    }
}
