package controleurs;

import interfaces.ConsulterVente;
import interfaces.util.BoutonIcone;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurBoutonIconePhoto implements EventHandler<ActionEvent> {

    private ConsulterVente vue;

    public ControleurBoutonIconePhoto(ConsulterVente vue)
    {
        this.vue = vue;
    }


    @Override
    public void handle(ActionEvent actionEvent) {

        BoutonIcone photo = (BoutonIcone)actionEvent.getSource();
        this.vue.setPhoto(photo.getIcone());

    }
}
