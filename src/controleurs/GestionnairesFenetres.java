package controleurs;

import interfaces.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import modeles.Conversation;
import modeles.Vente;

import java.util.ArrayList;
import java.util.List;

public class GestionnairesFenetres
{
    private static Scene main;
    public static Stage STAGE;
    private static List<InterfaceModeConnecte> pagesParcourues = new ArrayList<>();

    public static void initStage(Stage stage)
    {
        Scene laScene = new Scene(new ConnexionVue(), 800, 600);
        main = laScene;

        STAGE = stage;
        STAGE.setScene(main);
        STAGE.setTitle("VAE : Ventes Aux Enchères");
        STAGE.setResizable(false);
        STAGE.show();
    }

    public static void afficherVente(Vente vente)
    {
        afficherInterfaceConnectee(new ConsulterVente(vente));
    }

    public static void afficherMesAcquisitions()
    {
        supprimerPages();
        afficherInterfaceConnectee(new MesAcquisitions());
    }

    public static void afficherMesPropositions()
    {
        supprimerPages();
        afficherInterfaceConnectee(new MesPropositions());
    }

    public static void afficherMesVentes()
    {
        supprimerPages();
        afficherInterfaceConnectee(new MesVentes());
    }

    public static void afficherLesVentes()
    {
        supprimerPages();
        afficherInterfaceConnectee(new RechercherObjet());
    }

    public static void afficherAccueil()
    {
        supprimerPages();
        afficherInterfaceConnectee(new AccueilVue());
    }

    public static void afficherMessagerie()
    {
        afficherInterfaceConnectee(new Messagerie());
    }

    public static void afficherAjouterVente(int id) { afficherInterfaceConnectee(new AjouterVente(-1)); }

    public static void afficherConversation(Conversation conv)
    {
        afficherInterfaceConnectee(new ConsulterConversation(conv));
    }

    public static void afficherInterfaceConnexion()
    {
        afficherInterface(new ConnexionVue());
        supprimerPages();
    }

    public static void afficherInterfaceInscription()
    {
        afficherInterface(new CreerCompte());
        supprimerPages();
    }

    public static void afficherAjouterObjet(int id) { afficherInterfaceConnectee(new AjouterObjet(id));}

    public static void afficherDernierePage()
    {
        pagesParcourues.get(pagesParcourues.size() - 1).stop();
        pagesParcourues.remove(pagesParcourues.size() - 1);
        afficherInterfaceConnectee(pagesParcourues.get(pagesParcourues.size() - 1));
        pagesParcourues.get(pagesParcourues.size() - 1).stop();
        pagesParcourues.remove(pagesParcourues.size() - 1);
    }

    private static void afficherInterfaceConnectee(InterfaceModeConnecte ui)
    {
        pagesParcourues.add(ui);
        afficherInterface(ui.getInterface());
    }

    private static void afficherInterface(Parent parent)
    {
        main.setRoot(parent);
    }

    private static void supprimerPages()
    {
        for(InterfaceModeConnecte p : pagesParcourues)
            p.stop();

        pagesParcourues.clear();
    }
}
