package controleurs;

import interfaces.MesVentes;
import interfaces.util.BoutonIcone;
import interfaces.util.BoutonIconeTexte;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

import java.awt.*;


public class ControleurAcceAjouterVente implements EventHandler<ActionEvent> {

    private MesVentes mv;

    public ControleurAcceAjouterVente(MesVentes mv){
        this.mv = mv;
    }

    @Override
    public void handle(ActionEvent actionEvent){
        GestionnairesFenetres.afficherAjouterVente(-1);
    }
}
