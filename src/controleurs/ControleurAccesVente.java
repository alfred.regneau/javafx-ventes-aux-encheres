package controleurs;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import modeles.Vente;

public class ControleurAccesVente implements EventHandler<MouseEvent> {
    private Vente vente;

    public ControleurAccesVente(Vente vente)
    {
        this.vente = vente;
    }

    public void handle(MouseEvent e)
    {
        GestionnairesFenetres.afficherVente(vente);
    }
}
