package controleurs;

import interfaces.MesVentes;
import interfaces.util.BoutonIcone;
import interfaces.util.EntreeTableView;
import interfaces.util.EntreeTableViewMesVentes;
import interfaces.util.Util;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import modeles.Statut;
import modeles.Vente;

import java.util.Optional;

public class ControleurTableViewMesVentes implements EventHandler<ActionEvent>
{

    private Vente vente;
    private MesVentes vue;

    public ControleurTableViewMesVentes(Vente v, MesVentes vue)
    {
        this.vente = v;
        this.vue = vue;
    }


    @Override
    public void handle(ActionEvent actionEvent) {
        if (actionEvent.getSource() instanceof Hyperlink)
        {
            Hyperlink lien = (Hyperlink) actionEvent.getSource();
            GestionnairesFenetres.afficherVente(vente);
        }

        else if (actionEvent.getSource() instanceof BoutonIcone)
        {
            Image image = ((BoutonIcone) actionEvent.getSource()).getIcone();
            if (image == EntreeTableViewMesVentes.ICONE_VALIDER)
            {
                Optional<ButtonType> confirmation = Util.alert(Alert.AlertType.CONFIRMATION, "Confirmation de suppresion", "Cette vente va être supprimée", "Êtes vous sur de vouloir supprimer cette vente ?");
                if (confirmation.get() == ButtonType.OK)
                {
                    //TODO valider la vente
                    vente.setStatutVente(new Statut(4, "Validée"));
                }
            }
            else if(image == EntreeTableViewMesVentes.ICONE_POUBELLE)
            {
                Optional<ButtonType> confirmation = Util.alert(Alert.AlertType.CONFIRMATION, "Confirmation de suppresion", "Cette vente va être supprimée", "Êtes vous sur de vouloir supprimer cette vente ?");
                if (confirmation.get() == ButtonType.OK)
                {
                    vente.setStatutVente(new Statut(5, "Non conclue"));
                }
            }
            else if(image == EntreeTableViewMesVentes.ICONE_EDITER)
            {
                GestionnairesFenetres.afficherAjouterObjet(vente.getObjet().getId());
            }
            vue.getInterface();

        }


    }
}
