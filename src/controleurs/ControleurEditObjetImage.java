package controleurs;

import interfaces.AjouterObjet;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.FileChooser;

import java.io.File;

public class ControleurEditObjetImage implements EventHandler<ActionEvent> {
    private FileChooser fc;
    private int id;
    private AjouterObjet page;

    public ControleurEditObjetImage(int id, AjouterObjet page)
    {
        this.fc = new FileChooser();
        this.fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Images", "*.png", "*.jpg", "*.jpeg"));
        this.id = id;
        this.page = page;
    }

    public void handle(ActionEvent e)
    {
        File file = this.fc.showOpenDialog(GestionnairesFenetres.STAGE);
        if(file != null)
            page.changeImage(this.id, file);
    }
}
