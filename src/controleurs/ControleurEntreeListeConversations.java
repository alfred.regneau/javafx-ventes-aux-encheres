package controleurs;

import interfaces.util.EntreeListeConversations;
import interfaces.util.Styles;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class ControleurEntreeListeConversations implements EventHandler<MouseEvent>
{
    private EntreeListeConversations vue;

    public ControleurEntreeListeConversations(EntreeListeConversations vue)
    {
        this.vue = vue;
    }

    @Override
    public void handle(MouseEvent mouseEvent)
    {
        if(mouseEvent.getEventType() == MouseEvent.MOUSE_CLICKED)
        {
            GestionnairesFenetres.afficherConversation(this.vue.getConversation());
        }
        else if(mouseEvent.getEventType() == MouseEvent.MOUSE_ENTERED)
        {
            this.vue.setBackground(Styles.HOVER_BACKGROUND);
        }
        else if(mouseEvent.getEventType() == MouseEvent.MOUSE_EXITED)
        {
            this.vue.setBackground(Styles.DEFAULT_BACKGROUND);
        }
    }
}
