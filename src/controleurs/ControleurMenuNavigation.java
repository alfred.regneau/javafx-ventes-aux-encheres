package controleurs;

import interfaces.MenuNavigation;
import interfaces.util.BoutonIcone;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import modeles.ConnexionUtilisateur;


public class ControleurMenuNavigation implements EventHandler<ActionEvent>
{
    @Override
    public void handle(ActionEvent actionEvent)
    {
        BoutonIcone bouton = (BoutonIcone) actionEvent.getTarget();
        Image icone = bouton.getIcone();

        if(icone == MenuNavigation.ICONE_MESACQUISITIONS)
        {
            GestionnairesFenetres.afficherMesAcquisitions();
        }
        else if(icone == MenuNavigation.ICONE_MESPROPOSITIONS)
        {
            GestionnairesFenetres.afficherMesPropositions();
        }
        else if(icone == MenuNavigation.ICONE_VENTES)
        {
            GestionnairesFenetres.afficherLesVentes();
        }
        else if(icone == MenuNavigation.ICONE_HOME)
        {
            GestionnairesFenetres.afficherAccueil();
        }
        else if(icone == MenuNavigation.ICONE_DECONNEXION)
        {
            ConnexionUtilisateur.deconnexion();
            GestionnairesFenetres.afficherInterfaceConnexion();
        }
        else if(icone == MenuNavigation.ICONE_MESVENTES)
        {
            GestionnairesFenetres.afficherMesVentes();
        }
        else if(icone == MenuNavigation.ICONE_MESSAGERIE)
        {
            GestionnairesFenetres.afficherMessagerie();
        }
    }
}
