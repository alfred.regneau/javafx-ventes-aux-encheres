package controleurs;

import exceptions.IntrouvableException;
import exceptions.ResultatVideException;
import interfaces.ConsulterVente;
import interfaces.util.Util;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import modeles.ConnexionUtilisateur;
import modeles.Proposition;
import modeles.Utilisateur;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Optional;

public class ControleurBoutonEncherir implements EventHandler<ActionEvent> {

    private ConsulterVente vue;

    public ControleurBoutonEncherir(ConsulterVente vue)
    {
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent actionEvent)
    {
        TextInputDialog tid = new TextInputDialog();
        tid.setTitle("Choix du montant");
        tid.setHeaderText("Choisissez un montant pour enchérir");
        tid.setContentText("Montant (€) : ");
        Optional<String> result = tid.showAndWait();

        double montant = 0.0;

        try
        {
            if(!result.isPresent())
                throw new NoSuchElementException();

            montant = Double.parseDouble(result.get());

            if(montant <= this.vue.getVente().getPrixBase())
            {
                Util.alert(Alert.AlertType.INFORMATION, "Montant invalide", "Montant trop bas", "Veuillez renseigner un montant supérieur ou égal au prix de base de l'objet");
                return;
            }

            try
            {
                Proposition prop = Requetes.recupererPropositionMaximumPourVente(GestionnaireSQL.getConnexion(), this.vue.getVente().getId());

                if(prop.getMontant() >= montant)
                {
                    Util.alert(Alert.AlertType.INFORMATION, "", "Montant trop bas", "Le montant que vous avez sélectionné est plus bas que la meilleure enchère. Veuillez choisir un montant plus élevé.");
                    return;
                }
            }
            catch (ResultatVideException e) { /* tout se passe normalement */ }

            Requetes.ajouterProposition(GestionnaireSQL.getConnexion(),
                                        this.vue.getVente().getId(),
                                        ConnexionUtilisateur.getIdUtilisateurConnecte(),
                                        montant,
                                        new Date());

            Util.alert(Alert.AlertType.CONFIRMATION, "Enchère émise", "Enchère réussie", "Vous avez enchéri pour un montant de " + montant + "€");

            this.vue.rafraichir();
        }
        catch(NumberFormatException e)
        {
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : montant invalide !", "Veuillez entrer un montant ne contenant que des chiffres.");
        }
        catch(NoSuchElementException e)
        {
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : montant invalide !", "Veuillez entrer un montant.");
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur de lecture dans la BD", "Une erreur est survenue lors de la lecture d'informations dans la base de données.");
        }
    }
}
