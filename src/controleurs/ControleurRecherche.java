package controleurs;

import interfaces.util.IRecherche;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class ControleurRecherche implements EventHandler<ActionEvent>
{
    IRecherche page;

    public ControleurRecherche(IRecherche page)
    {
        this.page = page;
    }

    @Override
    public void handle(ActionEvent actionEvent)
    {
        this.page.recherche(this.page.getSearch().getText());
    }
}
