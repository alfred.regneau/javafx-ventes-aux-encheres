package controleurs;

import exceptions.EmailDejaPrisException;
import exceptions.IntrouvableException;
import exceptions.MotDePasseIncorrectException;
import exceptions.PseudoDejaPrisException;
import interfaces.CreerCompte;
import interfaces.util.Util;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Hyperlink;
import modeles.ConnexionUtilisateur;
import modeles.Role;
import modeles.Utilisateur;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;

public class ControleurInscription implements EventHandler<ActionEvent>
{
    private CreerCompte vue;

    public ControleurInscription(CreerCompte vue)
    {
        this.vue = vue;

    }

    @Override
    public void handle(ActionEvent actionEvent)
    {

        if(actionEvent.getSource() instanceof Hyperlink)
            GestionnairesFenetres.afficherInterfaceConnexion();

        boolean peutContinuer = true;

        this.vue.reinitSublabels();

        if(this.vue.getPseudo().equals(""))
        {
            this.vue.identifiantVide();
            peutContinuer = false;
        }

        if(this.vue.getMdp().equals(""))
        {
            this.vue.motDePasseVide();
            peutContinuer = false;
        }

        if(!this.vue.getMdp().equals(this.vue.getConfirmMdp()))
        {
            this.vue.confirmationNeCorrespondPas();
            peutContinuer = false;
        }

        if(!peutContinuer)
            return;


        // TODO Roles définis statiquement et donc Roles.DEFAUT (ou un nom du style)
        Utilisateur user = new Utilisateur(-1, this.vue.getPseudo(), this.vue.getEmail(), this.vue.getMdp(), true, new Role(2, "Utilisateur"));

        try
        {
            Requetes.ajouterUtilisateur(GestionnaireSQL.getConnexion(), user);
            ConnexionUtilisateur.connexion(user.getPseudo(), user.getMotDePasse());
            GestionnairesFenetres.afficherAccueil();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Une erreur est survenue", "Erreur de lecture dans la BD, redémarrez l'application et réessayez.");
        }
        catch (PseudoDejaPrisException e)
        {
            this.vue.identifiantDejaPris();
            return;
        }
        catch (EmailDejaPrisException e)
        {
            this.vue.emailDejaPris();
            return;
        }
        catch (MotDePasseIncorrectException | IntrouvableException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : impossible de se connecter", "Une erreur a eu lieu lors de la connexion automatique. Veuillez essayer de vous connecter manuellement.");
        }
    }
}
