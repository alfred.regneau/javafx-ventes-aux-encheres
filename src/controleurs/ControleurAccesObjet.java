package controleurs;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class ControleurAccesObjet implements EventHandler<MouseEvent> {
    private int id;

    public ControleurAccesObjet(int id)
    {
        this.id = id;
    }

    public void handle(MouseEvent e)
    {
        GestionnairesFenetres.afficherAjouterObjet(this.id);
    }
}
