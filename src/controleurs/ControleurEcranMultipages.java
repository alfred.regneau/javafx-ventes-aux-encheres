package controleurs;

import interfaces.InterfaceMultipages;
import interfaces.util.BarreMultipages;
import interfaces.util.BoutonIcone;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurEcranMultipages implements EventHandler<ActionEvent> {
    private InterfaceMultipages page;

    public ControleurEcranMultipages(InterfaceMultipages page)
    {
        this.page = page;
    }

    public void handle(ActionEvent e)
    {
        BoutonIcone b = (BoutonIcone)e.getSource();
        if(b.getIcone() == BarreMultipages.ICONE_GAUCHE)
            this.page.changePage(-1);
        else if(b.getIcone() == BarreMultipages.ICONE_DROITE)
            this.page.changePage(1);
        this.page.setChangePage(true);
    }
}
