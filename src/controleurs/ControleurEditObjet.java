package controleurs;

import interfaces.AjouterObjet;
import interfaces.util.BoutonIconeTexte;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurEditObjet implements EventHandler<ActionEvent> {
    private AjouterObjet page;

    public ControleurEditObjet(AjouterObjet page)
    {
        this.page = page;
    }

    public void handle(ActionEvent e)
    {
        BoutonIconeTexte b = (BoutonIconeTexte)e.getSource();
        if(b.getIcone() == AjouterObjet.ICONE_RETOUR)
            GestionnairesFenetres.afficherDernierePage();
        else if(b.getIcone() == AjouterObjet.ICONE_SAUVEGARDE)
        {
            this.page.sauvegarde();
            GestionnairesFenetres.afficherDernierePage();
        }
        else if(b.getIcone() == AjouterObjet.ICONE_AJOUTER)
            this.page.ajouterImage();
    }
}
