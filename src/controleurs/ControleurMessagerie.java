package controleurs;

import exceptions.IntrouvableException;
import interfaces.Messagerie;
import interfaces.util.BoutonIcone;
import interfaces.util.Util;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.control.Alert;
import modeles.ConnexionUtilisateur;
import modeles.Message;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;

public class ControleurMessagerie implements EventHandler<ActionEvent>
{
    private Messagerie vue;

    public ControleurMessagerie(Messagerie vue)
    {
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent actionEvent)
    {
        EventTarget cible = actionEvent.getTarget();

        if(cible instanceof BoutonIcone)
        {
            BoutonIcone btn = (BoutonIcone)cible;

            if(btn.getIcone() == Messagerie.ICONE_NOUVEAU_MSG)
            {
                //GestionnairesFenetres.afficherNouveauMessage();
                try {
                    GestionnairesFenetres.afficherConversation(Requetes.recupererConversationsUtilisateur(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte()).get(0));
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (IntrouvableException e) {
                    e.printStackTrace();
                }
            }
            else if(btn.getIcone() == Messagerie.ICONE_SUPPRIMER_MSG)
            {
                Util.alert(Alert.AlertType.WARNING, "Impossible", "Action non implémentée", "Cette action n'a pas été implémentée et est donc impossible à réaliser.");
            }
        }
    }
}
