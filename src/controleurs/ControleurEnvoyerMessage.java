package controleurs;

import exceptions.IntrouvableException;
import interfaces.ConsulterConversation;
import interfaces.util.Util;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import modeles.ConnexionUtilisateur;
import modeles.Message;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;
import java.util.Date;

public class ControleurEnvoyerMessage implements EventHandler<ActionEvent>
{

    private ConsulterConversation vue;

    public ControleurEnvoyerMessage(ConsulterConversation vue)
    {
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent actionEvent)
    {
        String msg = this.vue.getTextTape().getText();

        if (msg.isEmpty()) return;

        try
        {
            Message newMessage = new Message(-1,
                    new Date(),
                    msg,
                    false,
                    this.vue.getConversation().getCorrespondant(),
                    Requetes.recupererUtilisateurParId(GestionnaireSQL.getConnexion(), ConnexionUtilisateur.getIdUtilisateurConnecte()));
            Requetes.ajouterNouveauMessage(GestionnaireSQL.getConnexion(), this.vue.getConversation(), newMessage);
            this.vue.getConversation().ajouterMessage(newMessage);
            this.vue.envoiMessage();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (IntrouvableException e)
        {
            e.printStackTrace();
            Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : utilisateur introuvable", "Impossible de trouver votre interlocuteur");
        }
    }
}
