package controleurs;

import exceptions.IntrouvableException;
import interfaces.ConsulterVente;
import interfaces.util.BoutonIcone;
import interfaces.util.Util;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import modeles.Conversation;
import modeles.Message;
import modeles.Utilisateur;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;
import java.util.ArrayList;

public class ControleurConsulterVente implements EventHandler<ActionEvent>
{

    private ConsulterVente vue;

    public ControleurConsulterVente(ConsulterVente vue){
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent actionEvent) {

        BoutonIcone btn = (BoutonIcone) actionEvent.getTarget();

        if(btn.getIcone() == ConsulterVente.ICONE_RETOUR)
        {
            GestionnairesFenetres.afficherDernierePage();
        }
        else if(btn.getIcone() == ConsulterVente.ICONE_SEND)
        {
            try
            {
                Utilisateur interlocuteur = Requetes.recupererProprietaireVente(GestionnaireSQL.getConnexion(), this.vue.getVente().getId());
                GestionnairesFenetres.afficherConversation(new Conversation(new ArrayList<Message>(), this.vue.getVente().getObjet(), interlocuteur));
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur de lecture dans la BD", "Erreur lors de la requête vers la base de données");
            }
            catch (IntrouvableException e)
            {
                e.printStackTrace();
                Util.alert(Alert.AlertType.ERROR, "Erreur", "Erreur : utilisateur introuvable", "Impossible de trouver l'utilisateur associé à cette vente.");
            }
        }
    }
}
