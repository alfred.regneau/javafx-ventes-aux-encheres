package controleurs;

import interfaces.AjouterObjet;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurEditObjetSupprImage implements EventHandler<ActionEvent> {
    private int id;
    private AjouterObjet page;

    public ControleurEditObjetSupprImage(int id, AjouterObjet page)
    {
        this.id = id;
        this.page = page;
    }

    public void handle(ActionEvent e)
    {
        this.page.supprimerImage(this.id);
    }
}
