package controleurs;

import interfaces.AjouterVente;
import interfaces.ConsulterVente;
import interfaces.InterfaceModeConnecte;
import interfaces.util.BoutonIconeTexte;
import interfaces.util.Util;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import modeles.Statut;
import modeles.Vente;
import modeles.sql.GestionnaireSQL;
import modeles.sql.Requetes;

import java.sql.SQLException;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static java.util.Objects.isNull;


public class ControleurAjouterVente implements EventHandler<ActionEvent>
{
    private AjouterVente ajoutVente;

    public ControleurAjouterVente(AjouterVente ajoutVente)
    {
        this.ajoutVente = ajoutVente;
    }

    @Override
    public void handle(ActionEvent actionEvent)
    {
        BoutonIconeTexte b = (BoutonIconeTexte)actionEvent.getSource();

        if(b.getIcone() == InterfaceModeConnecte.ICONE_SEND)
        {
            try
            {
                double prixres;
                double prixmin;
                try
                {
                    prixmin = Double.parseDouble(ajoutVente.getMaPTF().getText());
                    prixres = Double.parseDouble(ajoutVente.getObjP().getText());
                }
                catch (NumberFormatException e)
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Format de nombre incorrect. Veuillez verifier si les sommes sont sous la forme X.X");
                    return;
                }

                LocalDate localDate = ajoutVente.getDateDeb().getValue();
                if (localDate == null)
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Veuillez choisir une date.");
                    return;
                }


                Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
                Date datedeb = Date.from(instant);
                Scanner sc = new Scanner(ajoutVente.getHeureDeb().getText());
                sc.useDelimiter(":");

                try
                {
                    int h = sc.nextInt();
                    int m = sc.nextInt();
                    if (h < 0 || h > 23 || m < 0 || m > 59)
                    {
                        Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Format d'heure incorrect. Veuillez verifier si l'heure est sous la forme HH:MM");
                        return;
                    }
                    datedeb.setTime(datedeb.getTime() + (h * 3600 + m * 60) * 1000);
                }
                catch (InputMismatchException e)
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Format d'heure incorrect. Veuillez verifier si l'heure est sous la forme HH:MM");
                    return;
                }
                catch (NoSuchElementException e)
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Veuillez entrer une heure.");
                    return;
                }


                LocalDate localDate2 = ajoutVente.getDateFin().getValue();
                if (localDate2 == null)
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Veuillez choisir une date.");
                    return;
                }
                Instant instant2 = Instant.from(localDate2.atStartOfDay(ZoneId.systemDefault()));
                Date datefin = Date.from(instant2);
                Scanner sc2 = new Scanner(ajoutVente.getHeureFin().getText());
                sc2.useDelimiter(":");
                try
                {
                    int h2 = sc2.nextInt();
                    int m2 = sc2.nextInt();
                    if (h2 < 0 || h2 > 23 || m2 < 0 || m2 > 59)
                    {
                        Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Format d'heure incorrect. Veuillez verifier si l'heure est sous la forme HH:MM");
                        return;
                    }
                    datefin.setTime(datefin.getTime() + (h2 * 3600 + m2 * 60) * 1000);
                }
                catch (InputMismatchException e)
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Format d'heure incorrect. Veuillez verifier si l'heure est sous la forme HH:MM");
                    return;
                }
                catch (NoSuchElementException e)
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Veuillez entrer une heure.");
                    return;
                }

                LocalDateTime d1 = LocalDateTime.ofInstant(datedeb.toInstant(), ZoneId.systemDefault());
                LocalDateTime d2 = LocalDateTime.ofInstant(datefin.toInstant(), ZoneId.systemDefault());
                Date current = new Date();

                if (isNull(ajoutVente.getAllObj().getValue()))
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Veuillez choisir un objet.");
                    return;
                }
                if (isAfterDay(datedeb, datefin))
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Les dates choisies ne conviennent pas.");
                    return;
                }

                if (!isAfterDay(datefin, current))
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Les dates choisies ne conviennent pas.");
                    return;
                }

                if (isSameDay(datedeb, datefin))
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Les dates choisies ne conviennent pas.");
                    return;
                }

                System.out.println(d1 + " " + d2);
                System.out.println(ChronoUnit.DAYS.between(d1, d2) > 7);

                if (ChronoUnit.DAYS.between(d1, d2) > 7)
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "D'après les dates choisies, votre enchère dure plus de 7 jours.");
                    return;
                }

                if (prixres <= prixmin || prixmin < 0 || prixres < 0)
                {
                    Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Le prix de réserve doit forcément être plus grand que le prix de base");
                    return;
                }
                else
                {
                    Vente newVente = new Vente(-1, prixmin, prixres, datedeb, datefin, ajoutVente.getAllObj().getValue(), new Statut(1, "à venir"));
                    Requetes.ajouterNouvelleVente(GestionnaireSQL.getConnexion(), newVente);
                    GestionnairesFenetres.afficherVente(newVente);
                }
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                Util.alert(Alert.AlertType.ERROR, "Message d'erreur", "Attention !", "Veuillez vérifier si vous avez rempli correctement les champs demandés.");
            }
        }

        else
            GestionnairesFenetres.afficherDernierePage();
    }


    private static boolean isAfterDay(Date date1, Date date2)
    {

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isAfterDay(cal1, cal2);
    }

    private static boolean isAfterDay(Calendar cal1, Calendar cal2)
    {

        if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA)) return false;
        if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA)) return true;
        if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) return false;
        if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) return true;
        return cal1.get(Calendar.DAY_OF_YEAR) > cal2.get(Calendar.DAY_OF_YEAR);
    }


    private static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    private static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }


}
