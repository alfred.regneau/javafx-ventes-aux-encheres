package controleurs;

import exceptions.IntrouvableException;
import exceptions.MotDePasseIncorrectException;
import interfaces.ConnexionVue;
import interfaces.util.Util;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import modeles.ConnexionUtilisateur;

import java.sql.SQLException;

public class ControleurConnexion implements EventHandler<ActionEvent>
{
    private ConnexionVue vue;

    public ControleurConnexion(ConnexionVue vue)
    {
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent actionEvent)
    {
        // ConnexionUtilisateur.connexion("mate73", "mate73");

        EventTarget target = actionEvent.getTarget();

        if (target instanceof Button || target instanceof TextField)
        {
            this.vue.reinitSublabels();

            try
            {
                ConnexionUtilisateur.connexion(this.vue.getNomUtilisateur(), this.vue.getMotDePasse());
                GestionnairesFenetres.afficherAccueil();
            }
            catch (IntrouvableException e)
            {
                this.vue.identifiantInconnu();
            }
            catch (MotDePasseIncorrectException e)
            {
                this.vue.motDePasseIncorrect();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                Util.alert(Alert.AlertType.ERROR, "Erreur", "Une erreur est survenue", "Erreur de lecture dans la BD, redémarrez l'application et réessayez.");
            }
        }
        
    }
}

