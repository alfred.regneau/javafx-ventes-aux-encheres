package controleurs;

import interfaces.CreerCompte;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class ListenerPseudoTextField implements ChangeListener<String> {

    private CreerCompte vue;

    public ListenerPseudoTextField(CreerCompte vue)
    {
        this.vue = vue;
    }

    @Override
    public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
        vue.verifiePseudo();
    }
}
