package controleurs;

import interfaces.InterfaceModeConnecte;
import interfaces.util.BoutonIcone;
import interfaces.util.IChangeMode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurLigneQuadrillage implements EventHandler<ActionEvent> {

    private IChangeMode vue;

    public ControleurLigneQuadrillage(IChangeMode vue)
    {
        this.vue = vue;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        BoutonIcone btn = (BoutonIcone) actionEvent.getTarget();
        if(btn.getIcone() == InterfaceModeConnecte.ICONE_LINE)
            this.vue.changeAffichage(false);

        else if(btn.getIcone() == InterfaceModeConnecte.ICONE_QUADRILLAGE)
            this.vue.changeAffichage(true);
    }
}
