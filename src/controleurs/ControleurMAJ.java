package controleurs;

import interfaces.InterfaceModeConnecte;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurMAJ implements EventHandler<ActionEvent>
{
    private InterfaceModeConnecte page;

    public ControleurMAJ(InterfaceModeConnecte page)
    {
        this.page = page;
    }

    @Override
    public void handle(ActionEvent actionEvent)
    {
        this.page.getInterface();
    }
}
